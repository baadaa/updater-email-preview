<img src="https://res.cloudinary.com/updater-marketing/image/upload/v1575574795/email_revamp/test-assets/ketchup-logo.png" width="100">

---

# Ketchup

Updater's internal tool to design, review, and expand email campaigns.

#### What it is

- A `Gatsby` site with each content module as a `React` component, which makes it easy to create a series of emails using the consistent visual structure.
- A series of static HTML/CSS pages with all email variations for real-time review, skipping laborious static design file back-and-forth.

#### What it isn't

- Poduction-ready HTML email template: no layout is structured in HTML tables, which is a requirement in emails.

---

## 🚀 Quick start

1.  **Start a local server**

    Once the repo cloned, navigate into your the repo folder and start it up (at the project root directory).

    ```shell
    npm start
    ```

1.  **Open the source code and start editing!**

    The site is now running at `http://localhost:8000`!

## 🧐 What's inside?

Look into `/src/components/` for individual content modules, `/src/pages/` for prebuilt email layouts, and `/src/styles/` for foundational base styles.

As a rule of thumb, email content modules are built with `(S)CSS` modules&mdash;_that is, external stylesheets_&mdash;for external sharing, while functional widget elements are built with `styled components` for easy access.

## TBD in README

- Template options explained
- Prop structure and convention explained
-
