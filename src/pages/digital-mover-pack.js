import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Digital Mover Pack',
  templateDescription: 'Promotional deals',
  // 2. HEADER AREA
  // 3. INTRO AREA
  recipientName: [true, 'Cookie Monster'],
  mainHeadline: [false, 'Save Hundreds on Moving Essentials'],
  mainSubtitle: [false, `Here is your Welcome Pack!`],
  introTextTitle: [true, `Save Hundreds on Moving Essentials`],
  introTextBody: [true, `Here is your Welcome Pack!`],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  moverPackBlock: [true],
  moverPackHighlight: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }
export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
