import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Welcome Email',
  templateDescription: 'The very first email from Updater',
  // WHEN/IF there are multiple options
  templateOptions: [
    { name: 'Option 1', link: '/welcome-email' },
    { name: 'Option 2', link: '/welcome-email-2' },
  ],
  // 2. HEADER AREA
  // 3. INTRO AREA
  recipientName: [true, 'Spongebob'],
  mainHeadline: [false, 'Welcome to Updater'],
  mainSubtitle: [false, 'Your move just got <em>a lot</em> easier.'],
  titleImage1: [
    false,
    'https://res.cloudinary.com/updater-marketing/image/upload/v1574795617/email_revamp/test-assets/welcome-emojies.png',
  ],
  titleImage2: [
    false,
    'https://res.cloudinary.com/updater-marketing/image/upload/v1574886419/email_revamp/test-assets/intro-illo.png',
  ],
  introTextTitle: [
    true,
    'Welcome to Updater &mdash; your move just got <em>a lot</em> easier.',
  ],
  introTextBody: [
    true,
    `We've broken your move into bite-sized tasks that we'll email you weekly
    to keep you on track. You'll save time, money, and a whole lot of
    headaches, we promise.`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  timelineBlock: [true],
  timelineBlockProminentUpFirst: [true],
  timeLineNextStepsHidden: [false],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [true],
  closingTextTitle: [true, 'Oh, and P.S.'],
  closingTextBody: [
    true,
    `Don't worry, we know our limits. We'll only email you with helpful
    content related to your upcoming move, and we'll stop once you're
    settled in.
    <br />
    <br />
    Sincerely,
    <br />
    <strong>Your friends at Updater</strong>`,
  ],
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
