import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Baby Come Back',
  templateDescription: 'Following up with inactive users',
  // 2. HEADER AREA
  // 3. INTRO AREA
  introTextTitle: [
    true,
    `Thanks for claiming your Updater account &mdash; now, let's keep you moving!`,
  ],
  introTextBody: [
    true,
    `As a reminder, [COMPANY NAME] has partnered with us to help you conquer your moving checklist in a fraction of the time. Log back into Updater today to:<br>
    <ul>
    <li>Shop for WiFi and TV</li>
    <li>Hook up your utilities</li>
    <li>Forward your mail</li>
    <li>And lots more...</li>
    </ul>`,
  ],
  introCTA: [true, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  // 4. SECONDARY CONTENT AREA
  // 5. COBRANDING AREA
  // 6. CLOSING CONTENT AREA
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
