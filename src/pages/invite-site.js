import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Invite - Site (Company)',
  templateDescription: 'Invite email with company co-branding',
  // 2. HEADER AREA
  partnerLogoHeader: [true],
  partnerID: 'indio',
  headerCTA: 'SIGN UP',
  // 3. INTRO AREA
  introTextTitle: [true, `Confirm your Updater account from Indio Management`],
  introTextBody: [
    true,
    `Activate your account today to receive important updates about your move and complete all of your moving-related tasks, including forwarding your mail and connecting internet & TV.`,
  ],
  introCTA: [true, 'Unlock My Account'],
  // 3. PRIMARY CONTENT AREA
  // 4. SECONDARY CONTENT AREA
  // 5. COBRANDING AREA
  cobrandingSite: [true],
  // 6. CLOSING CONTENT AREA
  // cobrandingPro: [true],
  // 7. FOOTER AREA
  footerLegalBlock: [false],
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
