import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Secure best offers on TV/Internet',
  templateDescription:
    'Abandonment flow, where users havce viewed the deals but not taken any actions.',
  // 2. HEADER AREA
  // 3. INTRO AREA
  recipientName: [false, 'Cookie Monster'],
  mainHeadline: [true, 'Secure the Best Offers on TV &amp; Internet'],
  mainSubtitle: [
    true,
    `Finding the best package takes time. 
  We've done the work for you. `,
  ],
  titleImage2: [
    true,
    'https://res.cloudinary.com/updater-marketing/image/upload/v1575501499/email_revamp/test-assets/isps.png',
  ],
  introTextBody: [
    true,
    `Check TV and internet off your list <em>in minutes</em>. Get a full view of the best offers on the market – no unwanted upsells or time wasted on hold with providers.`,
  ],
  introCTA: [true, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }
export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
