import React, { useState } from 'react'
import { Link } from 'gatsby'
import { Helmet } from 'react-helmet'
import '../styles/base.scss'
import styled from 'styled-components'

const MenuContainer = styled.div`
  max-width: 900px;
  margin: 0 auto;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  @keyframes pulse {
    0% {
      transform: rotate(5deg);
    }
    2% {
      transform: rotate(-5deg);
    }
    4% {
      transform: rotate(5deg);
    }
    6% {
      transform: rotate(0deg);
    }
    98% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(5deg);
    }
  }
  div#kcontainer {
    display: flex;
    align-items: center;
    justify-content: center;
    position: fixed;
    color: #fff;
    background: #333;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    transition: transform 0.2s;
    transform: translateY(100vh);
    p {
      max-width: 50vw;
      line-height: 1.4;
      font-size: 5vw;
      @media screen and (max-width: 860px) {
        font-size: 36px;
        max-width: 340px;
      }
      span {
        display: block;
        margin-top: 32px;
        font-size: 24px;
      }
    }
    &.on {
      transform: translateY(0vh);
    }
  }
`
const Logo = styled.div`
  position: fixed;
  bottom: 30px;
  right: 30px;
  width: 150px;
  height: 150px;
  border-radius: 200px;
  box-shadow: 0 2px 15px rgba(0, 0, 0, 0.15);
  z-index: 99;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  opacity: 0.85;
  cursor: pointer;
  transition: opacity 0.2s, transform 0.2s;
  img {
    animation: pulse 6s infinite;
    width: 100px;
    height: 100px;
    object-fit: contain;
  }
  &:hover {
    opacity: 1;
    transform: scale(1.03);
  }
  @media screen and (max-width: 900px) {
    transform: scale(0.8);
    transform-origin: bottom right;
    &:hover {
      opacity: 1;
      transform: scale(0.85);
    }
  }
`

const SectionHeading = styled.h2`
  padding-bottom: 5px;
  padding-top: 5px;
  font-size: 17px;
  color: #fff;
  color: ${props => (props.highlight ? '#2b7499' : '#39b3ca')};
  border-bottom: solid 2px;

  width: 100%;
  margin: 10px;
`
const Item = props => (
  <div {...props}>
    <Link to={props.to}>
      <h5>
        <span>{props.timing}</span>
        {props.title}
      </h5>
    </Link>
    <p>{props.desc}</p>
  </div>
)
const MenuBlock = styled(Item)`
  a {
    color: inherit;
    text-decoration: none;
    transition: transform 0.2s;
    &:hover {
      transform: ${props => (props.to ? 'scale(1.05)' : '')};
    }
  }
  box-sizing: border-box;
  margin: 10px;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  flex-direction: column;
  h5 {
    border-radius: 15px;
    overflow: hidden;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    position: relative;
    width: 150px;
    height: 150px;
    margin: 5px 0;
    font-weight: 200;
    font-size: 14px;
    line-height: 1.4;
    span {
      font-weight: 400;
    }
    background: ${props => (props.tbd ? '#f9f9f9' : '#fff')};
    box-shadow: ${props =>
      props.tbd ? 'inset 0 2px 7px #ddd' : '0 2px 7px #ddd'};
    &::before {
      display: ${props => (props.tbd ? 'block' : 'none')};
      content: 'TBD';
      font-size: 12px;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      box-sizing: border-box;
      padding: 5px 10px;
      background: #f0f0f0;
      border-bottom: 1px solid #ddd;
    }
  }
  p {
    margin: 5px 0;
    max-width: 150px;
    color: #999;
    font-size: 12px;
    line-height: 1.4;
  }
  position: relative;
`
export default () => {
  const [ketchupOn, setKetchupOn] = useState(false)
  return (
    <MenuContainer>
      <Helmet>
        <title>Ketchup: Updater Emails</title>
        <meta
          name="description"
          content="Newly revamped modular email templates for internal review and external sharing."
        />
        <meta
          property="og:description"
          content="Newly revamped modular email templates for internal review and external sharing."
        />
        <meta property="og:title" content="Ketchup: Updater Emails" />
        <meta name="twitter:title" content="Ketchup: Updater Emails" />
        <meta
          name="twitter:description"
          content="Newly revamped modular email templates for internal review and external sharing."
        />
        <meta
          property="og:image"
          content="https://res.cloudinary.com/updater-marketing/image/upload/v1575902949/email_revamp/test-assets/ketchup-card.png"
        />
        <meta
          name="twitter:image"
          content="https://res.cloudinary.com/updater-marketing/image/upload/v1575902949/email_revamp/test-assets/ketchup-card.png"
        />
        <meta
          name="image"
          content="https://res.cloudinary.com/updater-marketing/image/upload/v1575902949/email_revamp/test-assets/ketchup-card.png"
        />
      </Helmet>
      <SectionHeading>AT&T Pilot Drafts</SectionHeading>
      <MenuBlock
        to="/att-01"
        title="48-Hour Post-Claim"
        desc="Internet packages available at your new home"
      />
      <MenuBlock
        to="/att-02"
        title="12-day Pre-Move"
        desc="Ideal time to lock in internet installation"
      />
      <MenuBlock
        to="/att-03"
        title="Abandonment #1"
        desc="Viewed but not reserved"
      />
      <MenuBlock
        to="/att-04"
        title="Abandonment #2"
        desc="Viewed but not reserved"
      />
      <MenuBlock
        to="/att-05"
        title="Abandonment #3"
        desc="Viewed but not reserved"
      />
      <SectionHeading>Partner Communications</SectionHeading>
      <MenuBlock
        to="/monthly-metric"
        title="Monthly Metrics"
        desc="Monthly engagement reports for RE partners"
      />
      <SectionHeading>Invite Emails</SectionHeading>
      <MenuBlock
        to="/invite-updater-only"
        timing="Invite:"
        title="Updater Only"
        desc="Updater branding only"
      />
      <MenuBlock
        to="/invite-pro-only"
        timing="Invite:"
        title="Pro Only"
        desc="Updater + Person"
      />
      <MenuBlock
        to="/invite-site"
        timing="Invite:"
        title="Site"
        desc="Updater + Company"
      />
      <MenuBlock
        to="/invite-contextual"
        timing="Invite:"
        title="Contextual"
        desc="Updater + Property"
      />
      <MenuBlock
        to="/invite-cobranding"
        timing="Invite:"
        title="Co-branding"
        desc="Updater + Company + Property"
      />
      <MenuBlock
        to="/invite-professional"
        timing="Invite:"
        title="Professional"
        desc="Updater + Brokerage + Person"
      />
      <MenuBlock
        to="/invite-cobranded-professional"
        timing="Invite:"
        title="Co-branded Professional"
        desc="Updater + Brokerage + Two persons"
      />
      <SectionHeading>Lifecycle Emails</SectionHeading>
      <MenuBlock
        to="/welcome-email"
        title="Welcome Email"
        desc="The first email from Updater"
      />
      <MenuBlock
        to="/baby-come-back"
        title="Baby Come Back"
        desc="Following up with inactive users"
      />
      <MenuBlock
        tbd="true"
        to="/"
        timing="D-24:"
        title="Budgeting For Your Move "
        desc="How to budget for your Move, with Even Financial promo."
      />
      <MenuBlock
        tbd="true"
        to="/"
        timing="D-15:"
        title="Home Prep"
        desc="Get your new home settled, with HomeAdvisor promo."
      />
      <MenuBlock
        to="/d-12-tv-internet"
        timing="D-12:"
        title="TV/Internet"
        desc="Installation slots are filling up."
      />
      <MenuBlock
        tbd="true"
        to="/"
        timing="D+2:"
        title="Protect Your Home "
        desc="Homeowners only, promoting ADT."
      />
      <SectionHeading>Transaction Emails</SectionHeading>
      <MenuBlock
        to="/home-perks"
        title="HomePerks Receipts"
        desc="Receipts and confirmations, UHS Legacy"
      />
      <MenuBlock
        to="/even-financials"
        title="Move Now, Pay Later"
        desc="Fiance with Even Financials"
      />
      <MenuBlock
        to="/one-last-step"
        title="One Last Step"
        desc="Confirmation number and call number"
      />
      <MenuBlock
        to="/digital-mover-pack"
        title="Digital Mover Pack"
        desc="Promotional deals"
      />
      <MenuBlock
        to="/secure-best-offers"
        title="Secure best offers on TV/Internet"
        desc="Abandonment flow"
      />
      <SectionHeading highlight>Template Skeleton</SectionHeading>
      <MenuBlock
        to="/master-template"
        title="Master Template"
        desc="View all content modules"
      />
      <Logo onClick={() => setKetchupOn(!ketchupOn)}>
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1575574795/email_revamp/test-assets/ketchup-logo.png"
          alt="Ketchup"
        />
      </Logo>
      <div id="kcontainer" className={ketchupOn ? 'on' : ''}>
        <p>
          We've got a lot of email ketchup to do here at Updater.
          <span>Come back again for more.</span>
        </p>
      </div>
    </MenuContainer>
  )
}
