import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Move Now, Pay Later',
  templateDescription: 'Finance your move with Even Financials',
  // 2. HEADER AREA
  // 3. INTRO AREA
  recipientName: [false, 'Cookie Monster'],
  mainHeadline: [true, `We've picked the best moving rewards card`],
  mainSubtitle: [
    true,
    `If you're tired of burning through your cash and don't want to sacrifice on your new dream home then this credit card is for you. Not only does it offer 0% APR into 2021, but it's the card that keeps on giving with no annual fee, 5X rewards every quarter and $150 statement credit within your first 90 Days*.<br><br>`,
  ],
  titleImage2: [
    true,
    'https://res.cloudinary.com/updater-marketing/image/upload/c_pad,h_246,w_480/v1575905622/email_revamp/test-assets/aboc-card.png',
  ],
  introTextBody: [
    false,
    `If you're tired of burning through your cash and don't want to sacrifice on your new dream home then this credit card is for you. Not only does it offer 0% APR into 2021, but it's the card that keeps on giving with no annual fee, 5X rewards every quarter and $150 statement credit within your first 90 Days*.`,
  ],
  introCTA: [true, 'Check It Out', 'center'],
  // 3. PRIMARY CONTENT AREA
  simpleTextBlock: [
    false,
    `<span style="font-size: 12px;">Description*:<br>
    <ul>
    <li>Earn $150 Statement Credit after you spend $1,200 on purchases within the first 90 days from account opening
    </li>
    <li>
    Earn 5x rewards on up to $1,500 in combined purchases each quarter in popular categories such as dining, groceries, travel, and gas
    </li>
    <li>
    0% Intro APR on Purchases and Balance Transfers for 12 months; after that the variable APR will be 14.65% - 24.65%, based on your creditworthiness

    </li>
    </ul></span>`,
  ],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [true],
  closingTextBlockUmark: [false],
  closingTextBlockCentered: [true],
  closingTextTitle: [false, 'Have some things left on your to-do list?'],
  closingTextBody: [
    true,
    `<a href="#">Log back into Updater</a> to claim more deals.`,
  ],

  // 7. FOOTER AREA
  footerLegalBlock: [
    true,
    `<div style="max-width: 480px; margin: 0 auto;">
    Description*:<br>
    <ul style="padding: 0 0 0 15px;">
    <li>Earn $150 Statement Credit after you spend $1,200 on purchases within the first 90 days from account opening
    </li>
    <li>
    Earn 5x rewards on up to $1,500 in combined purchases each quarter in popular categories such as dining, groceries, travel, and gas
    </li>
    <li>
    0% Intro APR on Purchases and Balance Transfers for 12 months; after that the variable APR will be 14.65% - 24.65%, based on your creditworthiness
    </li>
    </ul></div>`,
  ],
}

const renderOptions = { ...DefaultOptions, ...options }
export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
