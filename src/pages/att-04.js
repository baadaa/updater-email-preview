import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'AT&T Pilot: Abandonment #2',
  templateDescription: 'Viewed but not reserved',
  // WHEN/IF there are multiple options
  // 2. HEADER AREA
  partnerLogoHeader: [true],
  showLogin: [false],
  // 3. INTRO AREA
  recipientName: [true, 'Hi Christine'],
  mainHeadline: [false, 'Welcome to Updater'],
  mainSubtitle: [false, 'Your move just got <em>a lot</em> easier.'],
  introTextTitle: [
    true,
    `Don’t let WiFi setup be the last thing on your list.`,
  ],
  introTextBody: [
    true,
    `With all the things you need to take care of during a move, it’s easy for something to fall through the cracks. That’s why we’re here – we wouldn’t dare let that happen to you! So, don’t forget 🎗️...`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  timelineBlock: [true, 'tv'],
  timelineBlockProminentUpFirst: [true],
  timeLineNextStepsHidden: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [false],
  // 7. FOOTER AREA
  footerLegalBlock: [false],

  // 8. OTHER OPTIONS
  otherOptions: {
    pilotType: 'att_mdu',
    contentVariation: 'abandon02',
  },
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
