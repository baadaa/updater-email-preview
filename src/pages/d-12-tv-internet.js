import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'D-12: TV/Internet',
  templateDescription: 'Installation slots are filling up.',
  // WHEN/IF there are multiple options
  // 2. HEADER AREA
  // 3. INTRO AREA
  recipientName: [true, 'Spongebob'],
  mainHeadline: [false, 'Welcome to Updater'],
  mainSubtitle: [false, 'Your move just got <em>a lot</em> easier.'],
  introTextTitle: [
    true,
    `The ideal time to set-up your TV and internet service is...`,
  ],
  introTextBody: [
    true,
    `12 days before your move. Why? This timing ensures you can secure an installation slot on the date/time of your choice. Haven’t scheduled yours yet? We can help!`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  timelineBlock: [true, 'tv'],
  timelineBlockProminentUpFirst: [true],
  timeLineNextStepsHidden: [false],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [true],
  closingTextBlockUmark: [true],
  closingTextBlockCentered: [true],
  closingTextTitleSmall: [true],
  closingTextTitle: [true, `Have some things left on your to-do list?`],

  closingTextBody: [
    true,
    `Log back into Updater, where we can help you keep you on track<br><br><a href="#">LOG IN</a>`,
  ],
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
