import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'Monthly Metrics',
  templateDescription: 'Monthly engagement reports for Real Estate Partners',
  // 2. HEADER AREA
  // 3. INTRO AREA
  mainHeadline: [true, 'Bozzuto Hudson House', 'isMetric'],
  metricSubheading: [true, `Monthly Performance Summary`],
  metricDate: [true, 'April 2020'],
  // 3. PRIMARY CONTENT AREA
  metricContent: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. COBRANDING AREA
  // 6. CLOSING CONTENT AREA
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
