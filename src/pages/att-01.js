import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'AT&T Pilot: 48-hour Post-claim',
  templateDescription: 'Internet packages available at your new home',
  // 2. HEADER AREA
  partnerLogoHeader: [true],
  showLogin: [false],
  // 3. INTRO AREA
  recipientName: [true, 'Hi Christine'],
  mainHeadline: [false, 'Bozzuto Hudson House', 'isMetric'],
  introTextTitle: [
    true,
    `Can you imagine going <em>one day</em> without WiFi?`,
  ],
  introTextBody: [
    true,
    `Neither can 96% of Updater movers. When surveyed, our movers said <strong style="font-weight: 400;">having internet installed on or before move day was their #1 priority.</strong> So, here are some internet recommendations from your neighbors at [[PROPERTY]]...`,
  ],
  metricSubheading: [false, `Monthly Performance Summary`],
  metricDate: [false, 'April 2020'],
  // 3. PRIMARY CONTENT AREA
  metricContent: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. COBRANDING AREA
  // 6. CLOSING CONTENT AREA
  // 7. FOOTER AREA
  footerLegalBlock: [false],
  // 8. OTHER OPTIONS
  otherOptions: {
    pilotType: 'att_mdu',
    contentVariation: 'abandon02',
  },
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
