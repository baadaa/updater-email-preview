import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'HomePerks Receipt',
  templateDescription: 'Transactional Receipts from HomePerks',
  // 2. HEADER AREA
  homePerksLogoHeader: [true],

  // 3. INTRO AREA
  recipientName: [true, 'Hi Joyce'],
  mainHeadline: [true, 'Thank you for your order!'],
  introTextTitle: [false, `Thank you for your order!`],
  introTextBody: [
    true,
    `This is a confirmation email for your utility services request from Duke Energy and additional services from HomePerks.<br><br>

    The details of your order are listed below. Be sure to print this email for your records.`,
  ],
  introCTA: [false],
  // 3. PRIMARY CONTENT AREA
  homePerksBlock: [true],

  // 4. SECONDARY CONTENT AREA
  // 5. COBRANDING AREA
  // 6. CLOSING CONTENT AREA
  // 7. FOOTER AREA
  homePerksFooter: [true],
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
