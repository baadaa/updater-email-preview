import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'AT&T Pilot: Abandonment #1',
  templateDescription: 'Viewed but not reserved',
  // WHEN/IF there are multiple options
  // 2. HEADER AREA
  partnerLogoHeader: [true],
  showLogin: [false],
  // 3. INTRO AREA
  recipientName: [true, 'Hi Christine'],
  mainHeadline: [false, 'Welcome to Updater'],
  mainSubtitle: [false, 'Your move just got <em>a lot</em> easier.'],
  introTextTitle: [true, `It's time to set up your internet and TV service.`],
  introTextBody: [
    true,
    `Here's your friendly reminder that [[PROPERTY NAME]] recommends AT&T as <em>the best</em> provider for your new home. <br /><span style="margin-top: 10px; display: block;">☝️💻</span>`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  timelineBlock: [true, 'tv'],
  timelineBlockProminentUpFirst: [true],
  timeLineNextStepsHidden: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [false],
  // 7. FOOTER AREA
  footerLegalBlock: [false],

  // 8. OTHER OPTIONS
  otherOptions: {
    pilotType: 'att_mdu',
    contentVariation: 'abandon01',
  },
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
