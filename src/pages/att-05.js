import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'AT&T Pilot: Abandonment #3',
  templateDescription: 'Viewed but not reserved',
  // WHEN/IF there are multiple options
  // 2. HEADER AREA
  partnerLogoHeader: [true],
  showLogin: [false],
  // 3. INTRO AREA
  recipientName: [true, 'Hi Christine'],
  mainHeadline: [false, 'Welcome to Updater'],
  mainSubtitle: [false, 'Your move just got <em>a lot</em> easier.'],
  introTextTitle: [true, `Internet installation slots are filling up…`],
  introTextBody: [
    true,
    `We’re reaching out with one final note to remind you that open slots for internet installation at your new home are getting scarce. It’s better to book sooner rather than later… trust us 😐.`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  timelineBlock: [true, 'tv'],
  timelineBlockProminentUpFirst: [true],
  timeLineNextStepsHidden: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [false],
  // 7. FOOTER AREA
  footerLegalBlock: [false],

  // 8. OTHER OPTIONS
  otherOptions: {
    pilotType: 'att_mdu',
    contentVariation: 'abandon03',
  },
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
