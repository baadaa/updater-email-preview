import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'One Last Step (abandonment flow)',
  templateDescription: 'Confirmation number and call number',
  // 2. HEADER AREA
  // 3. INTRO AREA
  mainHeadline: [true, 'Almost finished &mdash; just one more step.'],
  mainSubtitle: [
    true,
    `To complete your transfer, give us a call at <a href='tel:8335437743'>(833) 543-7743</a>.`,
  ],
  introTextTitle: [
    false,
    `To complete your transfer, give us a call at <a href='tel:8335437743'>(833) 543-7743</a>.`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  callCTABlock: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [true],
  closingTextBlockUmark: [true],
  closingTextBlockCentered: [true],
  closingTextTitle: [true, 'Have some things left on your to-do list?'],
  closingTextTitleSmall: [true],
  closingTextBody: [
    true,
    `Log back into Updater, where we can help to keep you on track.<br><br><a href='#'>LOG IN</a>`,
  ],
  // 7. FOOTER AREA
}

const renderOptions = { ...DefaultOptions, ...options }
export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
