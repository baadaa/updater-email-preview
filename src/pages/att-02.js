import React from 'react'
import '../styles/base.scss'
import Layout from '../components/Layout'
import DefaultOptions from '../components/DefaultOptions'

const options = {
  // 1. TEMPLATE INFO
  templateTitle: 'AT&T Pilot: 12-day Pre-Move',
  templateDescription: 'Ideal time to lock in internet installation',
  // WHEN/IF there are multiple options
  // 2. HEADER AREA
  partnerLogoHeader: [true],
  showLogin: [false],
  // 3. INTRO AREA
  recipientName: [true, 'Hi Christine'],
  mainHeadline: [false, 'Welcome to Updater'],
  mainSubtitle: [false, 'Your move just got <em>a lot</em> easier.'],
  introTextTitle: [
    false,
    `The ideal time to set-up your TV and internet service is...`,
  ],
  introTextBody: [
    true,
    `Your move is 12 days away. Having studied (literally) millions of move — many into [[property]] — we've found that this is the ideal time to lock in your wifi/TV installation slot.<br /><br /><strong style="font-weight: 400; color: #202020;">The most installed provider at your community: AT&T</strong>`,
  ],
  introCTA: [false, 'Get Started'],
  // 3. PRIMARY CONTENT AREA
  timelineBlock: [true, 'tv'],
  timelineBlockProminentUpFirst: [true],
  timeLineNextStepsHidden: [true],
  // 4. SECONDARY CONTENT AREA
  // 5. CLOSING CONTENT AREA
  closingTextBlock: [true],
  closingTextBlockUmark: [false],
  closingTextBlockCentered: [false],
  closingTextTitleSmall: [false],
  closingTextTitle: [true, `P.S.`],

  closingTextBody: [
    true,
    `How's that moving checklist looking? We're here for you. <a href="#">Log back into Updater</a> and knock it all out.<br><br>Yours Truly,<br />Updater`,
  ],
  // 7. FOOTER AREA
  footerLegalBlock: [false],

  // 8. OTHER OPTIONS
  otherOptions: {
    pilotType: 'att_mdu',
    contentVariation: 'generic',
  },
}

const renderOptions = { ...DefaultOptions, ...options }

export default props => <Layout toggle={props.toggleElem} {...renderOptions} />
