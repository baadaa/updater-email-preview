/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import parse from 'html-react-parser'
import {
  TextBlockHeading,
  TextBlockBody,
  MetricSubheading,
} from '../TextBlocks/TextBlocks'
import CTA_BTN from '../CTA_Btn/CTA_Btn'
import './IntroSection.scss'

const MainHeadline = props => {
  const {
    mainHeadline: [visible, content, isMetric],
  } = props
  return (
    <h1
      className={
        isMetric === 'isMetric' ? 'MainHeadline MetricTitle' : 'MainHeadline'
      }
      style={{ display: visible ? 'block' : 'none' }}
    >
      {content ? parse(content) : ''}
    </h1>
  )
}
const MainSubheadline = props => {
  const {
    mainSubtitle: [visible, content],
  } = props
  return (
    <div
      className="MainSubheadline"
      style={{ display: visible ? 'block' : 'none' }}
    >
      {content ? parse(content) : ''}
    </div>
  )
}
const MetricDate = props => {
  const {
    metricDate: [visible, content],
  } = props
  return (
    <div
      className="MainSubheadline MetricDate"
      style={{ display: visible ? 'block' : 'none', marginBottom: '0' }}
    >
      {content ? parse(content) : ''}
    </div>
  )
}
const MainHeadlineImage = props => {
  const { visible, src, alt, show, hide } = props
  /* eslint-disable */
  return (
    <img
      className="MainHeadlineImage"
      style={{ display: visible ? 'inherit' : 'none' }}
      src={src}
      alt={alt}
      onClick={show}
      onMouseOut={hide}
    />
  )
}
const RecipientName = props => {
  const {
    recipientName: [visible, name],
  } = props
  return (
    <div
      className="RecipientName"
      style={{ display: visible ? 'block' : 'none' }}
    >
      {name},
    </div>
  )
}
export default props => {
  const {
    recipientName,
    mainHeadline,
    mainSubtitle,
    metricSubheading,
    metricDate,
    titleImage1: [img1Visible, img1Src],
    titleImage2: [img2Visible, img2Src],
    introTextTitle,
    introTextBody,
    introCTA,
  } = props
  return (
    <div
      className="IntroSection"
      style={{ paddingBottom: metricDate ? '24px' : '' }}
    >
      <RecipientName recipientName={recipientName} />
      <MainHeadline mainHeadline={mainHeadline} />
      <MainSubheadline mainSubtitle={mainSubtitle} />
      <MetricSubheading metricSubheading={metricSubheading} />
      <MetricDate metricDate={metricDate} />
      <MainHeadlineImage show={props.showInfo} hide={props.hideInfo} visible={img1Visible} src={img1Src} alt="Welcome!" />
      <MainHeadlineImage show={props.showInfo} hide={props.hideInfo} visible={img2Visible} src={img2Src} alt="Welcome!" />
      <TextBlockHeading textBlockHeading={introTextTitle} />
      <TextBlockBody textBlockBody={introTextBody} />
      <a href="#">
        <CTA_BTN CTA={introCTA} />
      </a>
    </div>
  )
}
