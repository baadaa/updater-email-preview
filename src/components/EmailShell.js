import React from 'react'

export default props => (
  <div
    className="EmailShell"
    style={{ marginTop: '40px', boxShadow: '0 0 10px rgba(0,0,0,.05)' }}
  >
    {props.children}
  </div>
)
