const DefaultOptions = {
  // NOTE:
  // Every module option has to be an array.
  // - 0th item is a REQUIRED boolean, dictating whether it's included in the template.
  // - 1th item is an OPTIONAL string, determining what the content should be.

  // 1. TEMPLATE INFO
  templateTitle: 'Template Title',
  templateDescription: 'Short Description',
  // templateOptions: [ // WHEN/IF there are multiple options
  //   { name: 'Option 1', link: '/welcome-email' },
  //   { name: 'Option 2', link: '/welcome-email-2' },
  // ],

  // 2. HEADER AREA
  updaterLogoHeader: [true],
  partnerLogoHeader: [false],
  showLogin: [true],
  homePerksLogoHeader: [false],
  separateHomePerks: [false],

  // 3. INTRO AREA
  recipientName: [false, 'Recipient Name'],
  mainHeadline: [false, 'This is the primary Heading. Can it be long?'],
  metricSubheading: [false, `Monthly Performance Summary`],
  metricDate: [false, 'April 2020'],

  mainSubtitle: [false, 'Optional primary subheading'],
  titleImage1: [
    false,
    'https://res.cloudinary.com/updater-marketing/image/upload/v1574795617/email_revamp/test-assets/welcome-emojies.png',
  ],
  titleImage2: [
    false,
    'https://res.cloudinary.com/updater-marketing/image/upload/v1574886419/email_revamp/test-assets/intro-illo.png',
  ],
  introTextTitle: [false, 'Intro text heading comes here'],
  introTextBody: [
    false,
    `This is an intro text block, which can contain a free flow of text. Potentially multiple paragraphs, maybe bullet lists.<br><ul><li>Apple</li><li>Banana</li><li>A longer item like a choo-choo train</li></ul>`,
  ],
  introCTA: [false, 'Top CTA here'],

  // 3. PRIMARY CONTENT AREA
  simpleTextBlock: [false, `Optional text block here. `],
  metricContent: [false],
  timelineBlock: [false],
  timelineBlockProminentUpFirst: [false],
  timeLineNextStepsHidden: [false],
  callCTABlock: [false],
  moverPackBlock: [false],
  moverPackHighlight: [false],
  homePerksBlock: [false],

  // 4. SECONDARY CONTENT AREA
  twoUpImageAndText: [false],
  twoUpImageAndTextContent1: [
    false,
    'https://picsum.photos/500/300?random=1',
    `<h3>Biltong cupim pork chop</h3>
    <p>
      Pork belly kevin ribeye ham hock spare ribs cupim, pork chop tri-tip
      tail prosciutto fatback porchetta landjaeger turducken pig.
      <a href="#">Chew here</a>
    </p>`,
  ],
  twoUpImageAndTextContent2: [
    false,
    'https://picsum.photos/500/300?random=2',
    `<h3>Bacon spare ribs flank</h3>
    <p>
      Prosciutto flank shoulder turducken short loin picanha ham kielbasa
      pork chop cow. Venison strip steak swine pork belly.
      <a href="#">Bite this</a>
    </p>`,
  ],
  oneUpImageAndText: [false],
  oneUpImageAndTextContent1: [
    false,
    'https://picsum.photos/300?random=3',
    `<h3>Picanha burgdoggen pork chop</h3>
    <p>
      Turducken tail jowl, pork loin kevin hamburger alcatra ham salami
      sausage swine short ribs. Pig pancetta jowl, tail pastrami flank
      tenderloin
      <a>Eat it</a>
    </p>`,
  ],
  oneUpImageAndTextContent2: [
    false,
    'https://picsum.photos/300?random=4',
    `<h3>Porchetta hamburger shank</h3>
    <p>
      Rump porchetta pastrami, jerky ball tip jowl ribeye flank strip
      steak swine tongue. Tri-tip bacon pancetta shoulder jowl pork chop
      filet mignon ham hock tongue kevin spare ribs hamburger sirloin.
      <a>Smell this</a>
    </p>`,
  ],
  oneUpImageAndTextContent3: [
    false,
    'https://picsum.photos/300?random=5',
    `<h3>Sirloin ball tip pastrami</h3>
    <p>
      Pork chop kielbasa ground round jowl beef ribs, jerky hamburger
      pancetta corned beef cow strip steak shoulder. Tail pork chop ribeye
      short loin.
      <a>Eat it</a>
    </p>`,
  ],
  oneUpImageAndTextSmall: [false],
  oneUpImageAndTextSmallContent1: [
    false,
    'https://picsum.photos/300?random=6',
    `<h3>Venison strip steak</h3>
    <p>
    Kielbasa cow jowl pork jerky sausage boudin, chislic tri-tip. Beef ribs chicken andouille tri-tip pancetta pastrami shoulder picanha burgdoggen fatback porchetta . 
      <a>Eat it</a>
    </p>`,
  ],
  oneUpImageAndTextSmallContent2: [
    false,
    'https://picsum.photos/300?random=7',
    `<h3>Bacon spare ribs flank</h3>
    <p>
    Pork belly kevin ribeye ham hock spare ribs cupim, pork chop tri-tip tail prosciutto fatback porchetta landjaeger turducken pig. Jowl meatloaf andouille spare ribs shank pancetta doner ham hock jerky.
      <a>Eat it</a>
    </p>`,
  ],
  oneUpImageAndTextSmallContent3: [
    false,
    'https://picsum.photos/300?random=8',
    `<h3>Picanha burgdoggen pork chop</h3>
    <p>
      Turducken tail jowl, pork loin kevin hamburger alcatra ham salami
      sausage swine short ribs. Pig pancetta jowl, tail pastrami flank
      tenderloin
      <a>Eat it</a>
      </p>`,
  ],

  // 5. CO-BRANDING AREA
  cobrandingPro: [false],
  cobrandingSite: [false],
  cobrandingContextual: [false],
  cobrandingCobranding: [false],
  cobrandingProfessional: [false],
  cobrandingCoProfessional: [false],

  // 6. CLOSING CONTENT AREA
  closingTextBlock: [false],
  closingTextBlockWhiteBg: [false],
  closingTextBlockUmark: [false],
  closingTextBlockCentered: [false],
  closingTextTitle: [false, 'Closing text block can have a title'],
  closingTextTitleSmall: [false],
  closingTextBody: [
    true,
    'Optional closing text block, which can serve a variety of purposes. Bacon ipsum dolor amet fatback tri-tip short ribs alcatra shankle picanha. Andouille ribeye ham rump pork loin, ball tip strip steak cow meatball corned beef doner sirloin drumstick turducken leberkas. Salami tri-tip porchetta rump.',
  ],

  // 7. FOOTER AREA
  poweredByUpdaterFooter: [false],
  footerLegalBlock: [true],
  homePerksFooter: [false],

  // 8. OTHER OPTIONS
  otherOptions: {},
}

export default DefaultOptions
