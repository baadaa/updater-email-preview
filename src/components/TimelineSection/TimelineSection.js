/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import parse from 'html-react-parser'
import CTA_BTN from '../CTA_Btn/CTA_Btn'
import './TimelineSection.scss'

const TimelineUpFirst = props => {
  const { prominent, children } = props
  return (
    <div className="highlightedBox">
      <div
        className={prominent ? 'TimelineUpFirst Prominent' : 'TimelineUpFirst'}
      >
        {children}
      </div>
    </div>
  )
}

const TimelineNextStep = props => {
  const { link, children, noLine } = props
  return (
    <React.Fragment>
      <div
        className={
          noLine
            ? 'TimelineNextStepConnector NoLine'
            : 'TimelineNextStepConnector'
        }
      >
        &nbsp;
      </div>
      <div className="highlightedBox">
        <div className="TimelineNextStep">
          <div className="TimelineNextStepDot">
            <img
              src="https://res.cloudinary.com/updater-marketing/image/upload/v1575404889/email_revamp/test-assets/timelineDot.png"
              alt=""
            />
          </div>
          <div className="TimelineNextStepText">
            <a href={link} style={{ width: '100%', display: 'block' }}>
              {children}
            </a>
          </div>
          <div className="TimelineNextStepArrow">&#10132;</div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default props => {
  const {
    timelineBlock: [visible, tv],
    timelineBlockProminentUpFirst: [prominentUpFirst],
    timeLineNextStepsHidden: [hidden],
    otherOptions,
    showInfo,
    hideInfo,
  } = props

  const isAttMdu = otherOptions.pilotType === 'att_mdu'

  const attLogo =
    'https://res.cloudinary.com/updater-marketing/image/upload/v1571064381/website/2019-d2c-direction/isp-logos/ATT.png'

  const attHeading = {
    generic: `Get up to $250 in<br />Visa<sup style="font-size: small;">®</sup>&nbsp;Reward&nbsp;Cards`,
    abandon01: `Receive up to $250 in Visa<sup style="font-size: small;">®</sup>&nbsp;Reward&nbsp;Cards`,
    abandon02: `Get up to $250 in<br />Visa<sup style="font-size: small;">®</sup>&nbsp;Reward&nbsp;Cards`,
    abandon03: `Book today, and get up to $250&nbsp;in&nbsp;Visa<sup style="font-size: small;">®</sup> Reward&nbsp;Cards`,
  }
  const attHeadingParsed = parse(
    attHeading[otherOptions.contentVariation] || ''
  )
  const attString = {
    generic: `Schedule installation through Updater and score big. <br />Browse offers & schedule yours today.`,
    abandon01: `Slots are filling up for the week of your move. Schedule installation through Updater today.`,
    abandon02: `Slots are filling up for the week of your move. <br />Browse offers & schedule yours now.`,
    abandon03: `Limited installation availability for the week of your move. Schedule yours before slots fill up.`,
  }
  const attCta = {
    generic: `Get Started`,
    abandon01: `View Plans`,
    abandon02: `View Plans`,
    abandon03: `View Plans`,
  }
  const ctaString = isAttMdu
    ? attCta[otherOptions.contentVariation]
    : 'Get Started'
  const genericHeading = tv ? 'Set Up WiFi Installation' : 'Move Your Stuff'
  const genericTVstring = `The installation slots are filling up for the week of your move. Get your schedule set up now.`

  const imgString = tv
    ? 'https://res.cloudinary.com/updater-marketing/image/upload/v1545074024/website/2018-revamp-assets/icons/app-features/icon-app-feature-tv-internet-circle.png'
    : 'https://res.cloudinary.com/updater-marketing/image/upload/v1574884606/email_revamp/test-assets/moving-icon.png'
  /* eslint-disable */

  return (
    <div
      className="TimelineSection"
      style={{ display: visible ? 'block' : 'none' }}
    >
      <TimelineUpFirst prominent={prominentUpFirst}>
        <img src={imgString} alt="" onClick={showInfo} onMouseOut={hideInfo} style={{display: isAttMdu ? 'none' : ''}} />
        <div className="TimelineUpFirstText">
          <div className="categoryLabel" style={{ marginTop: isAttMdu ? '12px':''}}>
            {isAttMdu ? 'Recommended Provider' : 'Up First'}
          </div>
          <img src={attLogo} alt="" onClick={showInfo} onMouseOut={hideInfo} style={{display: !isAttMdu ? 'none' : '', width: 'auto', marginBottom: '15px' }} />
          <div className="TextBlockHeading">
          {isAttMdu ? attHeadingParsed : genericHeading}
          </div>
          <div className="TextBlockBody">
            {isAttMdu && attString ? 
            parse(attString[otherOptions.contentVariation]) 
            : genericTVstring}
          </div>
          <a href="#">
            <CTA_BTN CTA={[true, ctaString]} />
          </a>
        </div>
      </TimelineUpFirst>
      <div style={{ display: hidden ? 'none' : 'block' }}>
        <div
          className={
            prominentUpFirst
              ? 'TimelineNextStepTitle NoLine'
              : 'TimelineNextStepTitle'
          }
        >
          <span className="categoryLabel">We can also help you</span>
        </div>
        <TimelineNextStep link="#" noLine={prominentUpFirst}>
          {tv ? 'Prep your new home' : 'Shop for WiFi and TV'}
        </TimelineNextStep>
        <TimelineNextStep link="#">Hook up your utilities</TimelineNextStep>
        <TimelineNextStep link="#">Forward your mail</TimelineNextStep>
        <span style={{ paddingLeft: '30px' }}>
          <CTA_BTN CTA={[true, 'View More']} />
        </span>
      </div>
    </div>
  )
}
