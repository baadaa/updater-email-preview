/* eslint-disable */
import React from 'react'
import parse from 'html-react-parser'
import './ImageAndTextBlocks.scss'

const TwoUpImageAndText = props => {
  const {
    twoUpImageAndText: [visible],
    twoUpImageAndTextContent1: [, image1, text1],
    twoUpImageAndTextContent2: [, image2, text2],
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className="TwoUpImageAndTextBlock"
      style={{
        display: visible ? '' : 'none',
      }}
    >
      <div className="TwoUpOneBlock">
        <img
          src={image1}
          alt="random"
          onClick={showInfo}
          onMouseOut={hideInfo}
        />
        {parse(text1)}
      </div>
      <div className="TwoUpOneBlock">
        <img
          src={image2}
          alt="random"
          onClick={showInfo}
          onMouseOut={hideInfo}
        />
        {parse(text2)}
      </div>
    </div>
  )
}

const OneUpImageAndText = props => {
  const {
    oneUpImageAndText: [visible],
    oneUpImageAndTextContent1: [, image1, text1],
    oneUpImageAndTextContent2: [, image2, text2],
    oneUpImageAndTextContent3: [, image3, text3],
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className="OneUpImageAndTextBlock"
      style={{ display: visible ? 'block' : 'none' }}
    >
      <div className="OneUpBlock">
        <div className="OneUpImage">
          <img
            src={image1}
            alt="random"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="OneUpText">{parse(text1)}</div>
      </div>
      <div className="OneUpBlock">
        <div className="OneUpImage">
          <img
            src={image2}
            alt="random"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="OneUpText">{parse(text2)}</div>
      </div>
      <div className="OneUpBlock">
        <div className="OneUpImage">
          <img
            src={image3}
            alt="random"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="OneUpText">{parse(text3)}</div>
      </div>
    </div>
  )
}

const OneUpImageAndTextSmall = props => {
  const {
    oneUpImageAndTextSmall: [visible],
    oneUpImageAndTextSmallContent1: [, image1, text1],
    oneUpImageAndTextSmallContent2: [, image2, text2],
    oneUpImageAndTextSmallContent3: [, image3, text3],
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className="OneUpImageAndTextSmallBlock"
      style={{ display: visible ? 'block' : 'none' }}
    >
      <div className="OneUpSmallBlock">
        <div className="OneUpSmallImage">
          <img
            src={image1}
            alt="random"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="OneUpSmallText">{parse(text1)}</div>
      </div>
      <div className="OneUpSmallBlock">
        <div className="OneUpSmallImage">
          <img
            src={image2}
            alt="random"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="OneUpSmallText">{parse(text2)}</div>
      </div>
      <div className="OneUpSmallBlock">
        <div className="OneUpSmallImage">
          <img
            src={image3}
            alt="random"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="OneUpSmallText">{parse(text3)}</div>
      </div>
    </div>
  )
}

export { TwoUpImageAndText, OneUpImageAndText, OneUpImageAndTextSmall }
