/* eslint-disable */
import React from 'react'
// import { TextBlockBody, TextBlockSubheading } from '../TextBlocks/TextBlocks'
import './CobrandingSection.scss'

export default props => {
  const {
    cobrandingPro: [pro],
    cobrandingSite: [site],
    cobrandingContextual: [contextual],
    cobrandingCobranding: [cobranding],
    cobrandingProfessional: [professional],
    cobrandingCoProfessional: [coprofessional],
    showInfo,
    hideInfo,
  } = props
  const headshot = {
    person1:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/person1.png',
    person2:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681807/email_revamp/test-assets/person2.png',
  }
  const logo = {
    indio:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/indio-logo.png',
    ava:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/ava-newport-logo.png',
    movement:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/movement-mortgage-logo.png',
    ti:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/ti-logo.png',
    sunset:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681807/email_revamp/test-assets/sunset-lake-logo.png',
    nexthome:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681807/email_revamp/test-assets/nexthome-logo.png',
  }

  return (
    <>
      <div
        className="CobrandingSection"
        id="pro"
        style={{ display: pro ? 'block' : 'none' }}
      >
        <h6 className="CobrandingBroughtBy">Brought to you by:</h6>
        <div className="CobrandingContainer">
          <div className="CobrandingPersonBlock">
            <div className="CobrandingPerson">
              <img
                className="CobrandingPersonHeadshot"
                src={headshot.person2}
                alt=""
                onClick={showInfo}
                onMouseOut={hideInfo}
              />
              <div className="CobrandingPersonalInfo">
                <span className="CobrandingPersonName">Celicia Laird</span>
                <span className="CobrandingPersonDescription">
                  Realty One Group Iconic
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="CobrandingSection"
        id="site"
        style={{ display: site ? 'block' : 'none' }}
      >
        <h6 className="CobrandingBroughtBy">Brought to you by:</h6>
        <div className="CobrandingContainer">
          <div className="CobrandingLogoBlock">
            <img
              className="CobrandingLogo"
              src={logo.indio}
              alt="logo"
              onClick={showInfo}
              onMouseOut={hideInfo}
            />
          </div>
        </div>
      </div>
      <div
        className="CobrandingSection"
        id="contextual"
        style={{ display: contextual ? 'block' : 'none' }}
      >
        <h6 className="CobrandingBroughtBy">
          Brought to you by your community:
        </h6>
        <div className="CobrandingContainer">
          <div className="CobrandingLogoBlock">
            <img
              className="CobrandingLogo"
              src={logo.ava}
              alt="logo"
              onClick={showInfo}
              onMouseOut={hideInfo}
            />
          </div>
        </div>
      </div>
      <div
        className="CobrandingSection"
        id="cobranding"
        style={{ display: cobranding ? 'block' : 'none' }}
      >
        <h6 className="CobrandingBroughtBy">Brought to you by:</h6>
        <div className="CobrandingContainer">
          <div className="CobrandingLogoBlock">
            <img
              className="CobrandingLogo"
              src={logo.ti}
              alt="logo"
              onClick={showInfo}
              onMouseOut={hideInfo}
            />
          </div>
          <div className="CobrandingLogoBlock">
            <img
              className="CobrandingLogo"
              src={logo.sunset}
              alt="logo"
              onClick={showInfo}
              onMouseOut={hideInfo}
            />
          </div>
        </div>
      </div>
      <div
        className="CobrandingSection"
        id="professional"
        style={{ display: professional ? 'block' : 'none' }}
      >
        <h6 className="CobrandingBroughtBy">Brought to you by:</h6>
        <div className="CobrandingContainer">
          <div className="CobrandingLogoBlock">
            <img
              className="CobrandingLogo"
              src={logo.nexthome}
              alt="logo"
              onClick={showInfo}
              onMouseOut={hideInfo}
            />
          </div>
          <div className="CobrandingPersonBlock">
            <div className="CobrandingPerson">
              <img
                className="CobrandingPersonHeadshot"
                src={headshot.person2}
                alt=""
                onClick={showInfo}
                onMouseOut={hideInfo}
              />
              <div className="CobrandingPersonalInfo">
                <span className="CobrandingPersonName">Celicia Laird</span>
                <span className="CobrandingPersonDescription">
                  Realty One Group Iconic
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="CobrandingSection"
        id="co-professional"
        style={{ display: coprofessional ? 'block' : 'none' }}
      >
        <h6 className="CobrandingBroughtBy">Brought to you by:</h6>
        <div className="CobrandingContainer">
          <div className="CobrandingLogoBlock">
            <img
              className="CobrandingLogo"
              src={logo.movement}
              alt="logo"
              onClick={showInfo}
              onMouseOut={hideInfo}
            />
          </div>
          <div className="CobrandingPersonBlock">
            <div className="CobrandingPerson">
              <img
                className="CobrandingPersonHeadshot"
                src={headshot.person1}
                alt=""
                onClick={showInfo}
                onMouseOut={hideInfo}
              />
              <div className="CobrandingPersonalInfo">
                <span className="CobrandingPersonName">Amanda Wilkie</span>
                <span className="CobrandingPersonDescription">
                  Real Estate Professional
                </span>
              </div>
            </div>
            <div className="CobrandingPerson">
              <img
                className="CobrandingPersonHeadshot"
                src={headshot.person2}
                alt=""
                onClick={showInfo}
                onMouseOut={hideInfo}
              />
              <div className="CobrandingPersonalInfo">
                <span className="CobrandingPersonName">Celicia Laird</span>
                <span className="CobrandingPersonDescription">
                  Realty One Group Iconic
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

    // <div
    //   className={whiteBg ? 'ClosingTextSection WhiteBg' : 'ClosingTextSection'}
    //   style={{
    //     textAlign: isCentered ? 'center' : 'inherit',
    //     display: blockVisible ? 'block' : 'none',
    //   }}
    // >
    //   <div className="ClosingTextArea">
    //     <img
    //       src="https://res.cloudinary.com/updater-marketing/image/upload/v1476825365/logos/updater_U_logo_inverse_border.png"
    //       style={{
    //         display: showU ? 'block' : 'none',
    //         width: '50px',
    //         height: '50px',
    //         margin: '0 auto',
    //       }}
    //       alt=""
    //     />
    //     <TextBlockSubheading
    //       textBlockSubheadingSmall={closingTextTitleSmall}
    //       textBlockSubheading={closingTextTitle}
    //     />
    //     <TextBlockBody color="#333" textBlockBody={closingTextBody} />
    //   </div>
    // </div>
  )
}
