/* eslint-disable */
import React from 'react'
import CTA_BTN from '../CTA_Btn/CTA_Btn'
import './CallCTASection.scss'

const CallCTAContainer = props => (
  <div className="highlightedBox">
    <div className="CallCTAContainer">{props.children}</div>
  </div>
)

export default props => {
  const {
    callCTABlock: [visible],
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className="CallCTASection"
      style={{ display: visible ? 'block' : 'none' }}
    >
      <CallCTAContainer>
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1575476171/email_revamp/test-assets/call-icon.png"
          onClick={showInfo}
          onMouseOut={hideInfo}
          alt="Call"
        />
        <div className="TimelineUpFirstText">
          <div className="categoryLabel refNumber">Your Reference Number</div>
          <div className="TextBlockHeading">SJ017PBW</div>
          <div className="TextBlockBody">
            Need to make a change? No problem. Call us and let us know, and
            we'll help you find the best package.
          </div>
          <a href="#">
            <CTA_BTN CTA={[true, 'Call (833) 543-7743']} />
          </a>
        </div>
      </CallCTAContainer>
    </div>
  )
}
