/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import parse from 'html-react-parser'
import './MonthlyMetricSection.scss'
import CTA_BTN from '../CTA_Btn/CTA_Btn'

export default props => {
  const {
    metricContent: [isVisible],
    otherOptions,
  } = props
  const isAttMdu = otherOptions.pilotType === 'att_mdu'

  const metricContent = [
    {
      label: 'Clients Engaged',
      bigNumber: '37%',
      context: '(89 of 240)',
      average: '34%',
    },
    {
      label: 'Accounts Created',
      bigNumber: '55%',
      context: '(132 of 240)',
      average: '85%',
    },
    {
      label: 'Average # of Features Engaged',
      bigNumber: '3.2',
      context: '(Out of 8)',
      average: '4.5%',
    },
    {
      label: 'Average Time Saved per Client',
      bigNumber: '21',
      context: 'Hours',
      average: '29.5',
    },
  ]
  const attContent = [
    {
      label: 'Top Provider Special Offer',
      bigNumber: 'AT&T',
      logo:
        'https://res.cloudinary.com/updater-marketing/image/upload/v1571064381/website/2019-d2c-direction/isp-logos/ATT.png',
      context:
        '🍹 <strong style="font-weight: 400; color:#f5333f;">Summer special</strong>: Qualify for up to <strong style="font-weight: 400;">$250 in gift cards</strong>.<br /> <a href="#" style="margin-top: 5px; display: inline-block; color:#2b7499; font-weight: 400;" >Learn more</a>',
    },
    {
      label: 'Most Common Speed',
      bigNumber: '100 Mbps',
      logo: null,
      context:
        'Most residents choose internet plans with speeds of <strong style="font-weight: 400;">50&nbsp;Mbps &ndash; 1&nbsp;GIG</strong>',
    },
    {
      label: 'Most Popular Add-On',
      bigNumber: 'HBO Max',
      logo:
        'https://res.cloudinary.com/updater-marketing/image/upload/v1594664097/email_revamp/test-assets/hbo-max-black.png',
      context:
        'People have plenty of time for their favorite shows these days…',
    },
    {
      label: 'Lowest Internet Price',
      bigNumber: '$49.99/mo',
      logo: null,
      context: 'Plus snag low package prices when you bundle internet + TV',
    },
  ]
  const ctaDescription = isAttMdu
    ? 'Don’t get stuck without internet — browse your options, lock in installation, and save tons of 💸.'
    : 'To view the full metrics in details, log in to your company dashboard.'
  const ctaButtonText = isAttMdu ? 'View Packages' : 'Open Company Dashboard'
  const metricCell = ({ label, bigNumber, context, average }) => (
    <div className="MetricBlock">
      <div className="MetricLabel">{label}</div>
      <div className="MetricBigNumber">{bigNumber}</div>
      <div className="MetricContext">{context}</div>
      <div className="UpdaterAverage">
        Updater Average: <span className="ValueHighlight">{average}</span>
      </div>
    </div>
  )
  const attCell = ({ label, bigNumber, logo, context }) => (
    <div className="MetricBlock promo">
      <div className="MetricLabel">{label}</div>
      <div
        className="MetricBigNumber promo"
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '50px',
          boxSizing: 'border-box',
        }}
      >
        {logo ? (
          <img
            src={logo}
            alt=""
            style={{
              maxWidth: '100px',
              height: '50px',
              objectFit: 'contain',
            }}
          />
        ) : null}
        {logo ? null : bigNumber}
      </div>
      <div
        style={{
          height: '5px',
          width: '20px',
          margin: '3px auto 12px',
          backgroundColor: '#39b3ca',
          opacity: 0.3,
        }}
      ></div>
      <div className="UpdaterAverage promo" style={{ lineHeight: '1.5' }}>
        {parse(context)}
      </div>
    </div>
  )
  const metricTable = () => (
    <div className="MetricBlockContainer">
      <div className="MetricTableRow">
        {metricCell(metricContent[0])}
        <div className="MetricTableSpacerCell"></div>
        {metricCell(metricContent[1])}
      </div>
      <div className="MetricTableRow">
        {metricCell(metricContent[2])}
        <div className="MetricTableSpacerCell"></div>
        {metricCell(metricContent[3])}
      </div>
    </div>
  )
  const attTable = () => (
    <div className="MetricBlockContainer">
      <div className="MetricTableRow">
        {attCell(attContent[0])}
        <div className="MetricTableSpacerCell"></div>
        {attCell(attContent[1])}
      </div>
      <div className="MetricTableRow">
        {attCell(attContent[2])}
        <div className="MetricTableSpacerCell"></div>
        {attCell(attContent[3])}
      </div>
    </div>
  )
  return (
    <div style={{ display: isVisible ? '' : 'none' }}>
      {isAttMdu ? attTable() : metricTable()}
      <div
        className="MetricCtaArea"
        style={{ textAlign: isAttMdu ? 'left' : '' }}
      >
        {ctaDescription}
        <a href="#">
          <CTA_BTN CTA={[true, ctaButtonText]} />
        </a>
      </div>
    </div>
  )
}
