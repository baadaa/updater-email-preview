/* eslint-disable react/button-has-type */
import React from 'react'
import './EmailConfig.scss'

const ConfigItem = props => {
  const { id, click, checked, children, dataSection } = props
  return (
    <li data-section={dataSection}>
      <button id={id} onClick={click} type="button">
        <input type="checkbox" readOnly checked={checked} />
        {children}
      </button>
    </li>
  )
}

class EmailConfig extends React.Component {
  state = {
    expanded: false,
  }

  click = e => {
    const el = e.currentTarget.id
    this.props.toggleElem(el)
  }

  expand = () => {
    const tab = this.state.expanded
    this.setState({ expanded: !tab })
  }

  render() {
    const configItems = [
      [
        'HEADER AREA',
        { id: 'updaterLogoHeader', label: 'Updater Logo' },
        { id: 'partnerLogoHeader', label: 'Partner Logo' },
        { id: 'showLogin', label: 'Login Link' },
        { id: 'homePerksLogoHeader', label: 'HomePerks Branding' },
        { id: 'separateHomePerks', label: '- Separate partner logo' },
      ],
      [
        'INTRO AREA',
        { id: 'recipientName', label: 'Recipient Name' },
        { id: 'mainHeadline', label: 'Primary Heading' },
        { id: 'metricSubheading', label: 'Metric subheading' },
        { id: 'metricDate', label: 'Metric dates' },
        { id: 'mainSubtitle', label: 'Primary Subheading' },
        { id: 'titleImage1', label: 'Title Image #1' },
        { id: 'titleImage2', label: 'Title Image #2' },
        { id: 'introTextTitle', label: 'Intro Text Heading' },
        { id: 'introTextBody', label: 'Intro Text Body' },
        { id: 'introCTA', label: 'Intro CTA' },
      ],
      [
        'Primary CONTENT Area',
        { id: 'simpleTextBlock', label: 'General Text Block' },
        { id: 'metricContent', label: 'Monthly Metric Block' },
        { id: 'timelineBlock', label: 'Timeline Block' },
        {
          id: 'timelineBlockProminentUpFirst',
          label: ' - Prominent "Up First"',
        },
        { id: 'timeLineNextStepsHidden', label: ' - Hide "Next Steps"' },
        { id: 'callCTABlock', label: 'Call CTA Block' },
        { id: 'moverPackBlock', label: 'Mover Pack Block' },
        { id: 'moverPackHighlight', label: ' - Mover Pack Highlighted' },
        { id: 'homePerksBlock', label: 'HomePerks Block' },
      ],
      [
        'Secondary CONTENT Area',
        { id: 'twoUpImageAndText', label: 'Two-Up Image + Text' },
        { id: 'oneUpImageAndText', label: 'Image + Text' },
        { id: 'oneUpImageAndTextSmall', label: 'Image (small) + Text' },
      ],
      [
        'Co-Branding Area',
        { id: 'cobrandingPro', label: '+ Person' },
        { id: 'cobrandingSite', label: '+ Company' },
        { id: 'cobrandingContextual', label: '+ Property' },
        { id: 'cobrandingCobranding', label: '+ Company + Property' },
        { id: 'cobrandingProfessional', label: '+ Brokerage + Person' },
        { id: 'cobrandingCoProfessional', label: '+ Brokerage + 2 Persons' },
      ],
      [
        'Closing CONTENT Area',
        { id: 'closingTextBlock', label: 'Closing Text Block' },
        { id: 'closingTextBlockWhiteBg', label: ' - White Background' },
        { id: 'closingTextBlockUmark', label: ' - Updater U Circle' },
        { id: 'closingTextBlockCentered', label: ' - Center Closing Text' },
        { id: 'closingTextTitle', label: ' - Closing Text Heading' },
        { id: 'closingTextTitleSmall', label: ' - Smaller Size Heading' },
        { id: 'closingTextBody', label: ' - Closing Text Body' },
      ],

      [
        'Footer Area',
        { id: 'poweredByUpdaterFooter', label: 'Powered by Updater' },
        { id: 'footerLegalBlock', label: 'Footer Legal Copy' },
        { id: 'homePerksFooter', label: 'HomePerks Footer' },
      ],
    ]

    const toggleSection = target => {
      const section = target.closest('ul')
      const isExposed = section.dataset.exposed
      const toggleValue = isExposed === 'hide' ? 'show' : 'hide'
      section.setAttribute('data-exposed', toggleValue)
    }
    const sectionItems = (section, sectionIndex) =>
      section.map((item, index) => {
        if (typeof item === 'string') {
          return (
            <li
              className="ConfigSection"
              key={index}
              data-section={`section${sectionIndex}`}
            >
              <button
                type="button"
                onClick={e => toggleSection(e.currentTarget)}
              >
                {item}
              </button>
            </li>
          )
        }
        return (
          <ConfigItem
            id={item.id}
            key={item.id}
            click={this.click}
            dataSection={`section${sectionIndex}`}
            checked={
              typeof this.props[item.id] === 'object'
                ? this.props[item.id][0]
                : this.props[item.id]
            }
          >
            {item.label}
          </ConfigItem>
        )
      })

    return (
      <React.Fragment>
        <div
          id="WelcomeEmailConfig"
          className={this.state.expanded ? 'on' : ''}
        >
          {configItems.map((section, sectionIndex) => (
            <ul
              data-section={`section${sectionIndex}`}
              data-exposed="hide"
              key={sectionIndex}
            >
              {sectionItems(section, sectionIndex)}
            </ul>
          ))}
        </div>
        <button
          id="ConfigTab"
          onClick={this.expand}
          className={this.state.expanded ? 'on' : ''}
        >
          <img
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1575492492/email_revamp/test-assets/noun_config_1275200_trim.svg"
            alt=""
          />
        </button>
      </React.Fragment>
    )
  }
}

export default EmailConfig
