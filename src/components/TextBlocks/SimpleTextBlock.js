import React from 'react'
import parse from 'html-react-parser'
import './TextBlocks.scss'

export default props => {
  const {
    simpleTextBlock: [visible, content],
  } = props
  return (
    <div
      className="TextBlockBody SimpleBlock"
      style={{
        display: visible ? 'block' : 'none',
      }}
    >
      {content ? parse(content) : ''}
    </div>
  )
}
