import React from 'react'
import parse from 'html-react-parser'
import './TextBlocks.scss'

export const TextBlockBody = props => {
  const {
    textBlockBody: [visible, content],
    color,
  } = props
  return (
    <div
      className="TextBlockBody"
      style={{
        color: color || 'inherit',
        display: visible ? 'block' : 'none',
      }}
    >
      {content ? parse(content) : ''}
    </div>
  )
}

export const TextBlockHeading = props => {
  const {
    textBlockHeading: [visible, content],
  } = props
  return (
    <div
      className="TextBlockHeading"
      style={{ display: visible ? 'block' : 'none' }}
    >
      {content ? parse(content) : ''}
    </div>
  )
}

export const MetricSubheading = props => {
  const {
    metricSubheading: [visible, content],
  } = props
  return (
    <div
      className="TextBlockHeading MetricSubheading"
      style={{ display: visible ? 'block' : 'none' }}
    >
      {content ? parse(content) : ''}
    </div>
  )
}

export const TextBlockSubheading = props => {
  const {
    textBlockSubheading: [visible, content],
    textBlockSubheadingSmall: [isSmall],
  } = props
  return (
    <div
      className={isSmall ? 'TextBlockSubheading small' : 'TextBlockSubheading'}
      style={{ display: visible ? 'block' : 'none' }}
    >
      {content}
    </div>
  )
}
