import React from 'react'
import './CTA_Btn.scss'

export default props => {
  const {
    CTA: [visible, text],
  } = props
  return (
    <div
      style={{
        display: visible ? 'block' : 'none',
        textAlign: props.CTA[2] ? 'center' : '',
      }}
    >
      <button className="CTA_BTN" type="button">
        {text}
      </button>
    </div>
  )
}
