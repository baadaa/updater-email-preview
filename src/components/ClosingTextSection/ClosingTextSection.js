/* eslint-disable */
import React from 'react'
import { TextBlockBody, TextBlockSubheading } from '../TextBlocks/TextBlocks'
import './ClosingTextSection.scss'

export default props => {
  const {
    closingTextBlock: [blockVisible],
    closingTextBlockWhiteBg: [whiteBg],
    closingTextBlockCentered: [isCentered],
    closingTextBlockUmark: [showU],
    closingTextTitleSmall,
    closingTextTitle,
    closingTextBody,
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className={whiteBg ? 'ClosingTextSection WhiteBg' : 'ClosingTextSection'}
      style={{
        textAlign: isCentered ? 'center' : 'inherit',
        display: blockVisible ? 'block' : 'none',
      }}
    >
      <div className="ClosingTextArea">
        <img
          src="https://res.cloudinary.com/updater-marketing/image/upload/v1476825365/logos/updater_U_logo_inverse_border.png"
          style={{
            display: showU ? 'block' : 'none',
            width: '50px',
            height: '50px',
            margin: '0 auto',
          }}
          alt=""
          onClick={showInfo}
          onMouseOut={hideInfo}
        />
        <TextBlockSubheading
          textBlockSubheadingSmall={closingTextTitleSmall}
          textBlockSubheading={closingTextTitle}
        />
        <TextBlockBody color="#333" textBlockBody={closingTextBody} />
      </div>
    </div>
  )
}
