import React from 'react'
import { Link } from 'gatsby'
import '../../styles/base.scss'
import styled from 'styled-components'

const OptionMenuBar = styled.div`
  position: fixed;
  z-index: 99;
  top: 0;
  height: 40px;
  box-sizing: border-box;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  text-align: center;
  // justify-content: center;
  background: #eee;
  border-bottom: 1px solid #ccc;
  color: #333;
  box-shadow: 0 1px 5px #ddd;
  font-size: 12px;
  ul {
    display: flex;
    list-style: none;
    padding: 0;
    margin: 0;
  }
  a {
    color: inherit;
    text-decoration: none;
  }
`
const TemplateTitle = styled.h3`
  font-weight: 400;
  color: #000;
  margin: 0 10px;
  font-size: 12px;
`
const TemplateOption = styled.li`
  margin: 0 5px;
  a {
    border-bottom: 2px solid transparent;
    padding: 5px 0;
  }
`
const BackHome = styled(Link)`
  font-weight: 800;
  display: flex;
  align-items: center;
  background: #2b7499;
  color: #fff !important;
  align-self: stretch;
  padding: 0 10px;
  transition: background 0.2s;
  &:hover {
    background: #39b3ca;
  }
`

export default props => {
  const { templateTitle, templateOptions } = props
  const optionsList = templateOptions
    ? templateOptions.map((option, index) => (
        <TemplateOption key={index}>
          <Link
            to={option.link}
            activeStyle={{
              borderBottom: '2px solid #39b3ca',
            }}
          >
            {option.name}
          </Link>
        </TemplateOption>
      ))
    : ''
  return (
    <OptionMenuBar>
      <BackHome to="/">&lt; Go Back</BackHome>
      <TemplateTitle>{templateTitle}:</TemplateTitle>
      <ul>{optionsList}</ul>
    </OptionMenuBar>
  )
}
