import React from 'react'
import styled from 'styled-components'

const ImageInfo = styled.div`
  position: fixed;
  opacity: ${props => (props.visible ? 1 : 0)};
  transition: opacity 0.2s;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  pointer-events: none;
  z-index: 999;
  div {
    width: 340px;
    height: 160px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 27px;
    pointer-events: none;
    color: #fff;
    background: rgba(0, 0, 0, 0.85);
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
  }
`

const ImageInfoBlock = props => (
  <ImageInfo visible={props.visible}>
    <div>
      (w) {props.width}px (h) {props.height}px
    </div>
  </ImageInfo>
)
const showInfo = e => (
  <ImageInfo>
    {e.target.width}
    {e.target.height}
  </ImageInfo>
)

export { showInfo, ImageInfoBlock }
