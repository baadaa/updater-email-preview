/* eslint-disable */
import React from 'react'
import CTA_BTN from '../CTA_Btn/CTA_Btn'
import './MoverPackSection.scss'

const MoverPackHighlight = props => {
  const {
    moverPackHighlight: [visible],
    logoUrl,
    brandName,
    imgUrl,
    imgAlt,
    dealTitle,
    dealDesc,
    linkUrl,
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className="highlightedBox"
      style={{
        padding: '0px',
        marginBottom: '32px',
        display: visible ? 'block' : 'none',
      }}
    >
      <div className="MoverPackHighlight">
        <img
          className="MoverPackHighlightImg"
          src={logoUrl}
          onClick={showInfo}
          onMouseOut={hideInfo}
          alt={brandName}
        />
        <img
          className="MoverPackHighlightLogo"
          src={imgUrl}
          onClick={showInfo}
          onMouseOut={hideInfo}
          alt={imgAlt}
        />
        <div className="MoverPackHighlightContainer">
          <div className="categoryLabel brandName">{brandName}</div>
          <div className="TextBlockHeading">{dealTitle}</div>
          <div className="TextBlockBody">{dealDesc}</div>
          <a href={linkUrl}>
            <CTA_BTN CTA={[true, 'Shop']} />
          </a>
        </div>
      </div>
    </div>
  )
}

const MoverPackDeal = props => {
  const { logoUrl, brandName, dealDesc, showInfo, hideInfo } = props
  return (
    <React.Fragment>
      <div
        className="highlightedBox"
        style={{ marginBottom: '20px', padding: '12px' }}
      >
        <div className="MoverPackDeal">
          <img
            className="MoverPackDealLogo"
            onClick={showInfo}
            onMouseOut={hideInfo}
            src={logoUrl}
            alt={brandName}
          />
          <div className="MoverPackDealDetails">
            <div>
              <div className="MoverPackDealBrand categoryLabel">
                {brandName}
              </div>
              <div className="MoverPackDealDescription">{dealDesc}</div>
            </div>
            <CTA_BTN CTA={[true, 'Shop']} />
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default props => {
  const {
    moverPackBlock: [visible],
    moverPackHighlight,
    showInfo,
    hideInfo,
  } = props
  return (
    <div
      className="MoverPackSection"
      style={{ display: visible ? 'block' : 'none' }}
    >
      <MoverPackHighlight
        moverPackHighlight={moverPackHighlight}
        logoUrl="https://res.cloudinary.com/updater-marketing/image/upload/v1575486057/email_revamp/test-assets/bb-deal-sample.png"
        imgUrl="https://res.cloudinary.com/updater-marketing/image/upload/v1575486173/email_revamp/test-assets/bb-logo.png"
        imgAlt="Best Buy"
        brandName="Best Buy"
        dealTitle="Save up to $1,500 on 4K UHD TVs"
        dealDesc="Optional descriptive copy enticing people to click in."
        linkUrl="#"
        showInfo={showInfo}
        hideInfo={hideInfo}
      />
      <MoverPackDeal
        logoUrl="https://res.cloudinary.com/updater-marketing/image/upload/v1575486173/email_revamp/test-assets/bb-logo.png"
        brandName="Best Buy"
        showInfo={showInfo}
        hideInfo={hideInfo}
        dealDesc="Save up to $1,500 on 4K UHD TVs"
      />
      <MoverPackDeal
        logoUrl="https://res.cloudinary.com/updater-marketing/image/upload/v1575486173/email_revamp/test-assets/target-logo.png"
        brandName="Target"
        showInfo={showInfo}
        hideInfo={hideInfo}
        dealDesc="Save up to $1,500 on 4K UHD TVs"
      />
      <MoverPackDeal
        logoUrl="https://res.cloudinary.com/updater-marketing/image/upload/v1575486173/email_revamp/test-assets/ha-logo.png"
        brandName="Home Advisor"
        showInfo={showInfo}
        hideInfo={hideInfo}
        dealDesc="Save up to $1,500 on 4K UHD TVs"
      />
      <MoverPackDeal
        logoUrl="https://res.cloudinary.com/updater-marketing/image/upload/v1575486172/email_revamp/test-assets/shipt-logo.png"
        brandName="Shipt"
        showInfo={showInfo}
        hideInfo={hideInfo}
        dealDesc="$25 grocery credit, plus 1-month of fress same-day delivery"
      />
    </div>
  )
}
