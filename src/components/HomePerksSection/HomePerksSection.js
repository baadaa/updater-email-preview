/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import './HomePerksSection.scss'

const UserInfoContent = (
  <div className="HomePerksTableShell">
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Name</div>
      <div className="HomePerksTableContent">Joyce Cipolone</div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Email</div>
      <div className="HomePerksTableContent">Joyce.Cipolone@gmail.com</div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Address</div>
      <div className="HomePerksTableContent">
        2811 S Flamingo Rd, Apt 334
        <br />
        Avon Park, FL 33825
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Order Confirmation</div>
      <div className="HomePerksTableContent">ABC-123456678-987</div>
    </div>
  </div>
)

const UtilOrderContent = (
  <div className="HomePerksTableShell">
    <div className="HomePerksTableCompanyHeading">
      <img
        className="CompanyLogo"
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/duke-energy_2x.png"
        alt="Duke Energy"
      />
      <span className="ProductName">Utility Order Details</span>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">
        Service Requested Start&nbsp;Date
      </div>
      <div className="HomePerksTableContent">01/02/2020</div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Account Number</div>
      <div className="HomePerksTableContent">2230935487</div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Customer Service Phone</div>
      <div className="HomePerksTableContent">
        For questions on your utility service, call{' '}
        <span className="PhoneNumber">1&nbsp;(800)&nbsp;700.8744</span>
      </div>
    </div>
  </div>
)

const UpsellOrderContent = (
  <div className="HomePerksTableShell">
    <div className="HomePerksTableCompanyHeading">
      <img
        className="CompanyLogo"
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/duke-energy_2x.png"
        alt="Duke Energy"
      />
      <span className="ProductName">Surge Coverage & Grounding Premium</span>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Monthly Fees</div>
      <div className="HomePerksTableContent">
        <div className="PricingTable">
          <div className="PricingTableRow">
            <div className="PricingTableLabel">Standard Fee</div>
            <div className="PricingTableContent">$9.99/mo</div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">Total Monthly Fees</div>
            <div className="PricingTableContent">$9.99/mo</div>
          </div>
        </div>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">
        Estimated 1st&nbsp;Bill
        <span className="HomePerksTableLabelSub">
          (May exclude standard taxes & regulatory fees)
        </span>
      </div>
      <div className="HomePerksTableContent pricing big">$9.99</div>
    </div>
  </div>
)

const HomePerksOrderCenturyLinkContent = (
  <div className="HomePerksTableShell">
    <div className="HomePerksTableIntroCopy">
      Below is information regarding your digital services purchase. You will
      find the original installation instructions from the time of your order.
      If you already have an installation plan, it is not necessary to repeat
      this process.
      <u>
        Please note, changing the original service order may delay the
        installation of services and could result in a loss of any special
        promotional pricing.
      </u>
      <br />
      <br />
      Your order has been sent to CenturyLink for processing. You may be
      contacted if additional information is needed to complete your order.
      CenturyLink services and offers are not available everywhere. CenturyLink
      may change, cancel, or substitute offers and services, or vary them by
      service area at its sole discretion without notice. Services and
      subscriptions require credit approval and deposit may be required.
      <br />
      <br />
      If a term agreement applies to service offer, an early termination fee
      will apply: As determined by service location, an early termination fee
      will apply as either a flat $99 fee or the applicable monthly recurring
      service fee multiplied by the number of months remaining in the minimum
      service period, up to $200.
      <br />
      <br />
      Terms and Conditions: All products and services listed are go erned by
      tariffs, local terms of service, or terms and conditions posted to
      <a href="http://www.centurylink.com/Pages/AboutUs/Legal/TermsAndConditions/">
        http://www.centurylink.com/Pages/AboutUs/Legal/TermsAndConditions/
      </a>
      . Taxes, Fees and Surcharges: Taxes, fees, and surcharges apply, including
      a Carrier Universal Service charge, National Access Fee surcharge and
      state and local fees that vary by area and certain in-state surcharges.
      Cost recovery fees are not taxes or government-required charges use.
      Taxes, fees, and surcharges apply based on standard monthly, not
      promotional, rates.
      <br />
      <br />
      Thank you for choosing CenturyLink!
      <br />
      <br />
      1-877-816-8553.
    </div>
    <div className="HomePerksTableCompanyHeading">
      <img
        className="CompanyLogo"
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/century-link_2x.png"
        alt="Century Link"
      />
      <span className="ProductName">CenturyLink Internet 10M/1M</span>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Bonus</div>
      <div className="HomePerksTableContent Emphasis">
        $100 Mover Reward Card!*
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Monthly Fees</div>
      <div className="HomePerksTableContent">
        <div className="PricingTable">
          <div className="PricingTableRow">
            <div className="PricingTableLabel">Standard Fee</div>
            <div className="PricingTableContent">$49.00/mo</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">Advanced Modem Lease</div>
            <div className="PricingTableContent">$10.00/mo</div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">Total Monthly Fees</div>
            <div className="PricingTableContent">$59.00/mo</div>
          </div>
        </div>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">One-Time Fees</div>
      <div className="HomePerksTableContent">
        <div className="PricingTable">
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              Tech Installation with 1 computer
            </div>
            <div className="PricingTableContent">$99.00/mo</div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">Total One-Time Fees</div>
            <div className="PricingTableContent">$99.00/mo</div>
          </div>
        </div>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">
        Estimated 1st&nbsp;Bill
        <span className="HomePerksTableLabelSub">
          (May exclude standard taxes & regulatory fees)
        </span>
      </div>
      <div className="HomePerksTableContent pricing big">$158.00</div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Other Options</div>
      <div className="HomePerksTableContent">
        <span className="OtherOptionLabel">Installation Preference:</span>
        <span className="OtherOptionContent">Professional Installation</span>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Installation Scheduling</div>
      <div className="HomePerksTableContent">1/06/2020, 8:00 AM - 12:00 PM</div>
    </div>
    <div className="HomePerksTableFooter">
      *For instructions on processing your bonus{' '}
      <strong>($100 Mover Reward Card!*)</strong>, please{' '}
      <a href="#">click here</a>.
      <br />
      <br />
      Thank you for your CenturyLink order for{' '}
      <a href="#">2811 S Flamingo Rd, APT #334, Avon Park, FL 33825</a>.
    </div>
  </div>
)

const HomePerksOrderATTContent = (
  <div className="HomePerksTableShell">
    <div className="HomePerksTableIntroCopy">
      Thank you for the order. You will receive an order status email with the
      details of the order.
      <br />
      <br />
      &bull; <strong>DirecTV:</strong> If you rent your residence or live in a
      home owner’s association you will need to provide the technician with the
      completed “Landlord Permission” form in order for DirecTV to install the
      satellite dish. The link to the Landlord Permission form is included in
      the email (
      <a href="directv.com/landlordpermission">
        directv.com/landlordpermission
      </a>
      )<br />
      <br />
      &bull; <strong>AT&T</strong>: Before contacting AT&T, please note
      processing takes 24–48 hours. It may also be necessary for a
      representative to contact you before your order can be finalized. If you
      have any additional questions in regards to the order you may call
      844-776-0966.
    </div>
    <div className="HomePerksTableCompanyHeading">
      <img
        className="CompanyLogo"
        src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/att_2x.png"
        alt="AT&T"
      />
      <span className="ProductName">Internet 25 + U300 TV All Included</span>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Bonus</div>
      <div className="HomePerksTableContent Emphasis">
        $100 AT&T Visa&reg; Reward Card!*
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Monthly Fees</div>
      <div className="HomePerksTableContent">
        <div className="PricingTable">
          <div className="PricingTableRow">
            <div className="PricingTableSubheading">Base Monthly Fees</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">Rate</div>
            <div className="PricingTableContent">$184.00/mo</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">2 TVs Total</div>
            <div className="PricingTableContent">$10.00/mo</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">Broadcast TV Surcharge</div>
            <div className="PricingTableContent">$7.99/mo</div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">Total Base Monthly Fees</div>
            <div className="PricingTableContent">$201.99/mo</div>
          </div>
        </div>
        <div className="PricingTable">
          <div className="PricingTableRow">
            <div className="PricingTableSubheading">Introductory Rates</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              Price Adjustment
              <span className="PricingTableLabelNote">
                Beginning on February 1 the standard monthly price for High
                Speed Internet Basic will be $28, Express will be $36, Pro will
                be $41, Elite will be $46, Max will be $51, Max Plus will be
                $56, and Max Turbo will be $66.
              </span>
              <span className="PricingTableLabelNote">
                (For a limited time)
              </span>
            </div>
            <div className="PricingTableContent">&nbsp;</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              Broadcast TV Surcharge
              <span className="PricingTableLabelNote">
                Beginning on February 1 there will be a separate Broadcast TV
                Surcharge of up to $1.99 per month for all U-Verse TV customers.
              </span>
              <span className="PricingTableLabelNote">
                (For a limited time)
              </span>
            </div>
            <div className="PricingTableContent">&nbsp;</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              $5 off for 12 months when I enroll in Auto Bill Pay and Paperless
              Billing. Paperless Billing requires a valid email address.
              <span className="PricingTableLabelNote">(For 12 months)</span>
            </div>
            <div className="PricingTableContent">-$5.99/mo</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              $20 Off Internet 25Mbps - 12 month term for AT&T Internet required
              <span className="PricingTableLabelNote">(For 12 months)</span>
            </div>
            <div className="PricingTableContent">-$20.00/mo</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              $59 off for 12 months - 12 month term for U-verse TV required
              <span className="PricingTableLabelNote">(For 12 months)</span>
            </div>
            <div className="PricingTableContent">-$59.00/mo</div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">
              Total Introductory + Base Monthly Fees
            </div>
            <div className="PricingTableContent">$117.99/mo</div>
          </div>
        </div>
        <div className="PricingTable">
          <div className="PricingTableRow Subheading">
            <div className="PricingTableSubheading">
              Standard Rates <em>(After Introductory rates expire)</em>
            </div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">
              Total Standard Rates + Base Monthly Fees
            </div>
            <div className="PricingTableContent">$201.99/mo</div>
          </div>
        </div>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">One-Time Fees</div>
      <div className="HomePerksTableContent">
        <div className="PricingTable">
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              Price Adjustment
              <span className="PricingTableLabelNote">
                Beginning on February 1 the standard monthly price for High
                Speed Internet Basic will be $28, Express will be $36, Pro will
                be $41, Elite will be $46, Max will be $51, Max Plus will be
                $56, and Max Turbo will be $66.
              </span>
              <span className="PricingTableLabelNote">
                (For a limited time)
              </span>
            </div>
            <div className="PricingTableContent">&nbsp;</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              Broadcast TV Surcharge
              <span className="PricingTableLabelNote">
                Beginning on February 1 there will be a separate Broadcast TV
                Surcharge of up to $1.99 per month for all U-Verse TV customers.
              </span>
              <span className="PricingTableLabelNote">
                (For a limited time)
              </span>
            </div>
            <div className="PricingTableContent">&nbsp;</div>
          </div>
          <div className="PricingTableRow">
            <div className="PricingTableLabel">
              Required AT&T Activation Fee
            </div>
            <div className="PricingTableContent">$35.00</div>
          </div>
          <div className="PricingTableRow Total">
            <div className="PricingTableLabel">Total One-Time Fees</div>
            <div className="PricingTableContent">Total may vary</div>
          </div>
        </div>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">
        Estimated 1st&nbsp;Bill
        <span className="HomePerksTableLabelSub">
          (May exclude standard taxes & regulatory fees)
        </span>
      </div>
      <div className="HomePerksTableContent pricing big">Total may vary</div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Other Options</div>
      <div className="HomePerksTableContent">
        <span className="OtherOptionLabel">
          Would you like any of your receivers to be wireless?
        </span>
        <span className="OtherOptionContent">No Wireless Receivers</span>
        <span className="OtherOptionLabel">
          Watch your channels in High Definition (HD):
        </span>
        <span className="OtherOptionContent">High Definition (HD) Service</span>
        <span className="OtherOptionLabel">
          Video Programming Options-Select additional programming packages at a
          great low monthly fee:
        </span>
        <span className="OtherOptionContent">
          HBO/Cinemax Package - Free for three months
        </span>
        <span className="OtherOptionLabel">
          Subscribe to Internet and get AT&T ConnecTech Support&nbsp;Plus
        </span>
        <span className="OtherOptionContent">
          No, I don not want ConnecTech Support&nbsp;Plus
        </span>
      </div>
    </div>
    <div className="HomePerksTableRow">
      <div className="HomePerksTableLabel">Installation Scheduling</div>
      <div className="HomePerksTableContent">1/06/2020, 8:00 AM - 12:00 PM</div>
    </div>
    <div className="HomePerksTableFooter">
      *For instructions on processing your bonus{' '}
      <strong>($100 AT&T Visa&reg; Reward Card!*)</strong>, please{' '}
      <a href="#">click here</a>.
      <br />
      <br />
      Thank you for your AT&T order for{' '}
      <a href="#">2811 S Flamingo Rd, APT #334, Avon Park, FL 33825</a>.
    </div>
  </div>
)

const HomePerksTable = props => {
  const { tableTitle, content } = props
  return (
    <div className="HomePerksTable">
      <h3>{tableTitle}</h3>
      {content}
    </div>
  )
}

export default props => {
  const {
    homePerksBlock: [visible],
  } = props
  return (
    <div
      className="HomePerksSection"
      style={{ display: visible ? 'block' : 'none' }}
    >
      <HomePerksTable tableTitle="Your Information" content={UserInfoContent} />
      <HomePerksTable
        tableTitle="Duke Energy Utility Order Summary"
        content={UtilOrderContent}
      />
      <HomePerksTable
        tableTitle="Duke Energy Home Protection Plan Order Summary"
        content={UpsellOrderContent}
      />
      <HomePerksTable
        tableTitle="HomePerks Order Summary"
        content={HomePerksOrderCenturyLinkContent}
      />
      <HomePerksTable
        tableTitle="HomePerks Order Summary"
        content={HomePerksOrderATTContent}
      />
    </div>
  )
}
