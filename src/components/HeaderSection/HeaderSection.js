/* eslint-disable  */
import React from 'react'
import './HeaderSection.scss'

export default props => {
  const partners = {
    indio:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/indio-logo.png',
    ava:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/ava-newport-logo.png',
    movement:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/movement-mortgage-logo.png',
    ti:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681806/email_revamp/test-assets/ti-logo.png',
    sunset:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681807/email_revamp/test-assets/sunset-lake-logo.png',
    nexthome:
      'https://res.cloudinary.com/updater-marketing/image/upload/v1578681807/email_revamp/test-assets/nexthome-logo.png',
  }
  const {
    updaterLogoHeader: [updVisible],
    partnerLogoHeader: [partnerVisible],
    homePerksLogoHeader: [isHomePerks],
    separateHomePerks: [separateLogos],
    showLogin: [showLogin],
    headerCTA,
    partnerID,
    showInfo,
    hideInfo,
  } = props
  const partnerLogoString = (
    <img
      className="PartnerLogo"
      src={partnerID ? partners[partnerID] : partners.ava}
      alt="Avalon"
      onClick={showInfo}
      onMouseOut={hideInfo}
    />
  )
  const updaterLogoString = (
    <img
      className="UpdaterLogo"
      src="https://res.cloudinary.com/updater-marketing/image/upload/v1535143942/website/2018-revamp-assets/logos/updater-logo.png"
      alt="Updater"
      onClick={showInfo}
      onMouseOut={hideInfo}
    />
  )
  return (
    <>
      <div className="HeaderSection">
        <span
          className="LogoBlock"
          style={{ display: isHomePerks ? 'none' : '' }}
        >
          {updVisible ? updaterLogoString : ''}
          {partnerVisible ? partnerLogoString : ''}
        </span>
        <span
          className="LogoBlock"
          style={{
            display: !isHomePerks ? 'none' : 'flex',
            width: separateLogos ? '100%' : '',
            justifyContent: 'space-between',
          }}
        >
          <img
            className="PartnerLogo"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/homeperk-logo_2x.png"
            alt="HomePerks"
            style={{ marginRight: '18px', width: '160px' }}
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
          <img
            className="PartnerLogo"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/duke-energy_2x.png"
            alt="Duke Energy"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </span>
        <a href="#" style={{ display: isHomePerks || !showLogin ? 'none' : '' }}>
          {headerCTA || 'LOG IN'}
        </a>
      </div>
      <div
        className="HomePerksHeadingCTA"
        style={{ display: !isHomePerks ? 'none' : 'none' }}
      >
        For more information, call (844) 240.4386
      </div>
    </>
  )
}
