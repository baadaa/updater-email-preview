/* eslint-disable */
import React from 'react'
import './FooterSection.scss'
import parse from 'html-react-parser'

export const FooterSection = props => {
  const {
    poweredByUpdaterFooter: [poweredByVisible],
    footerLegalBlock: [legalVisible, legalContent],
    homePerksFooter: [homePerksFooter],
    showInfo,
    hideInfo,
  } = props
  const poweredByUpd = (
    <span
      className="categoryLabel"
      style={{ marginBottom: '16px', display: 'block' }}
    >
      Powered by
    </span>
  )
  return (
    <>
      <div
        className="FooterSection"
        style={{ display: homePerksFooter ? 'none' : '' }}
      >
        {poweredByVisible ? poweredByUpd : ''}

        <div className="FooterIconArea">
          <img
            className="UpdaterLogoFooter"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1535143942/website/2018-revamp-assets/logos/updater-logo.png"
            alt="Updater"
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
          <br />
          <img
            className="SocialIconFooter"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1546018284/website/2018-revamp-assets/icons/fb-gray-icon.png"
            alt=""
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
          <img
            className="SocialIconFooter"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1546018284/website/2018-revamp-assets/icons/twitter-gray-icon.png"
            alt=""
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
          <img
            className="SocialIconFooter"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1546018284/website/2018-revamp-assets/icons/linkedin-gray-icon.png"
            alt=""
            onClick={showInfo}
            onMouseOut={hideInfo}
          />
        </div>
        <div className="FooterPrimaryLinks">
          <a href="#">Login</a> | <a href="#">Moving Tips</a> |{' '}
          <a href="#">Help</a>
        </div>
        <div className="FooterSecondaryLinks">
          <a href="#">Unsubscribe</a> <a href="#">Privacy</a>{' '}
          <a href="#">Terms</a>
        </div>
        <div className="FooterOfficeAddress">
          <span>19 Union Square, 12th Floor</span> &bull;
          <span>New York, NY 10003</span> &bull;
          <span>
            <a href="https://updater.com">updater.com</a>
          </span>
        </div>
        <div
          className="FooterLegalBlock"
          style={{ display: legalVisible ? 'block' : 'none' }}
        >
          {legalContent
            ? parse(legalContent)
            : `Legal copy here. Lorem ipsum dolor sit amet, consectetur adipiscing
        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
        ut aliquip ex ea commodo consequat. Duis aute irure dolor in
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
        qui officia deserunt mollit anim id est laborum.`}
        </div>
      </div>
      <div
        className="FooterSection"
        style={{ display: !homePerksFooter ? 'none' : '' }}
      >
        <div className="FooterIconArea">
          <img
            className="HomePerksLogoFooter"
            src="https://res.cloudinary.com/updater-marketing/image/upload/v1582691571/email_revamp/test-assets/homeperk-logo_2x.png"
            alt="HomePerks"
          />
        </div>
        <div className="HomePerksByUpdater">
          Powered by <span>Updater</span>
        </div>
        <div className="HomePerksCallCenterInfo">
          For more information, call (844) 240.4386
          <span className="HomePerksCallCenterHours">Mon–Fri: 7am–1am EST</span>
          <span className="HomePerksCallCenterHours">
            Sat–Sun: 7am-11pm EST
          </span>
        </div>
        <div className="FooterPrimaryLinks HomePerks">
          <a href="#">TV/Internet</a> | <a href="#">Moving Tips</a> |{' '}
          <a href="#">Moving Guide</a> | <a href="#">Mover Coupons</a>
        </div>
        <div className="FooterLegalBlock HomePerks">
          HomePerks provides services to help connect your new home. HomePerks
          is powered by Bridgevine, Inc. Exclusions and limitations apply. See
          terms and conditions for details. These non-regulated utility products
          or services are administered by Duke Energy Florida, LLC located at
          550 South Tryon Street Charlotte, NC 28202 and provided by Service
          Plan of Florida, Inc. located at 175 W Jackson Blvd. Chicago, IL
          60604.
          <br />
          <br />
          Non-regulated utility products and services offered by Duke Energy are
          not regulated or sanctioned by the Florida Public Service Commission.
          Customers who purchase or subscribe to receive information about Duke
          Energy non-regulated products will not receive preferential or special
          treatment from their utility company and customers are not required to
          purchase or subscribe in order to receive safe, reliable electric
          service. THIS MESSAGE IS PAID FOR BY THE SHAREHOLDERS OF DUKE ENERGY.
          FL Lic # QB25213
          <br />
          <br />
          This message was sent to jcipolone@gmail.com from HomePerks. This is a
          service-related email from HomePerks, a Bridgevine, Inc. company.
          HomePerks will occasionally send you service-related emails to inform
          you of service upgrades or new benefits.
          <br />
          <br />
          Bridgevine, Inc., 5555 Glenridge Connector NE, 10th Floor, Atlanta, GA
          30342
          <br />
          e-duke-uoc-def-20200205 | © 2020 Bridgevine, Inc. All rights reserved.
        </div>
        <div className="FooterSecondaryLinks HomePerks">
          <a href="#">Unsubscribe</a> <a href="#">Privacy</a>
        </div>
        <div className="FooterOfficeAddress">
          <span>19 Union Square, 12th Floor</span> &bull;
          <span>New York, NY 10003</span>
        </div>
        <div className="FooterOfficeAddress">
          <span>5555 Glenridge Connector NE, 10th Floor</span> &bull;
          <span>Atlanta, GA 30342</span>
        </div>
        <div className="FooterOfficeAddress">
          <span>
            <a href="https://updater.com">updater.com</a>
          </span>
        </div>
      </div>
    </>
  )
}
