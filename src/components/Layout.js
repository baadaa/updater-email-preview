import React from 'react'
import '../styles/base.scss'
import { Helmet } from 'react-helmet'
import EmailShell from './EmailShell'
import OptionSelector from './OptionSelector/OptionSelector'
import HeaderSection from './HeaderSection/HeaderSection'
import IntroSection from './IntroSection/IntroSection'
import EmailConfig from './Configurator/EmailConfig'
import SimpleTextBlock from './TextBlocks/SimpleTextBlock'
import MonthlyMetricSection from './MonthlyMetricSection/MonthlyMetricSection'
import TimelineSection from './TimelineSection/TimelineSection'
import CallCTASection from './CallCTASection/CallCTASection'
import MoverPackSection from './MoverPackSection/MoverPackSection'
import HomePerksSection from './HomePerksSection/HomePerksSection'
import {
  TwoUpImageAndText,
  OneUpImageAndText,
  OneUpImageAndTextSmall,
} from './ImageAndTextBlocks/ImageAndTextBlocks'
import ClosingTextSection from './ClosingTextSection/ClosingTextSection'
import CobrandingSection from './CobrandingSection/CobrandingSection'
import { FooterSection } from './FooterSection/FooterSection'
import { ImageInfoBlock } from './Utils'

class Layout extends React.Component {
  constructor(props) {
    super(props)
    const {
      updaterLogoHeader,
      partnerLogoHeader,
      homePerksLogoHeader,
      separateHomePerks,
      showLogin,
      partnerID,
      headerCTA,
      recipientName,
      mainHeadline,
      mainSubtitle,
      metricSubheading,
      metricDate,
      titleImage1,
      titleImage2,
      introTextTitle,
      introTextBody,
      introCTA,
      simpleTextBlock,
      metricContent,
      timelineBlock,
      timelineBlockProminentUpFirst,
      timeLineNextStepsHidden,
      callCTABlock,
      moverPackBlock,
      moverPackHighlight,
      homePerksBlock,
      twoUpImageAndText,
      twoUpImageAndTextContent1,
      twoUpImageAndTextContent2,
      oneUpImageAndText,
      oneUpImageAndTextContent1,
      oneUpImageAndTextContent2,
      oneUpImageAndTextContent3,
      oneUpImageAndTextSmall,
      oneUpImageAndTextSmallContent1,
      oneUpImageAndTextSmallContent2,
      oneUpImageAndTextSmallContent3,
      closingTextBlock,
      closingTextBlockWhiteBg,
      closingTextBlockUmark,
      closingTextBlockCentered,
      closingTextTitle,
      closingTextTitleSmall,
      closingTextBody,
      cobrandingPro,
      cobrandingSite,
      cobrandingContextual,
      cobrandingCobranding,
      cobrandingProfessional,
      cobrandingCoProfessional,
      poweredByUpdaterFooter,
      footerLegalBlock,
      homePerksFooter,
      otherOptions,
    } = props
    this.state = {
      updaterLogoHeader,
      partnerLogoHeader,
      homePerksLogoHeader,
      separateHomePerks,
      showLogin,
      partnerID,
      headerCTA,
      recipientName,
      mainHeadline,
      mainSubtitle,
      metricSubheading,
      metricDate,
      titleImage1,
      titleImage2,
      introTextTitle,
      introTextBody,
      introCTA,
      simpleTextBlock,
      metricContent,
      timelineBlock,
      timelineBlockProminentUpFirst,
      timeLineNextStepsHidden,
      callCTABlock,
      moverPackBlock,
      moverPackHighlight,
      homePerksBlock,
      twoUpImageAndText,
      twoUpImageAndTextContent1,
      twoUpImageAndTextContent2,
      oneUpImageAndText,
      oneUpImageAndTextContent1,
      oneUpImageAndTextContent2,
      oneUpImageAndTextContent3,
      oneUpImageAndTextSmall,
      oneUpImageAndTextSmallContent1,
      oneUpImageAndTextSmallContent2,
      oneUpImageAndTextSmallContent3,
      closingTextBlock,
      closingTextBlockWhiteBg,
      closingTextBlockUmark,
      closingTextBlockCentered,
      closingTextTitle,
      closingTextTitleSmall,
      closingTextBody,
      cobrandingPro,
      cobrandingSite,
      cobrandingContextual,
      cobrandingCobranding,
      cobrandingProfessional,
      cobrandingCoProfessional,
      poweredByUpdaterFooter,
      footerLegalBlock,
      homePerksFooter,
      otherOptions,
      imgInfoVisible: false,
      imgWidth: 0,
      imgHeight: 0,
    }
  }

  bothLogosOffInHeader = el => {
    if (el !== 'updaterLogoHeader' && el !== 'partnerLogoHeader') {
      return
    }
    const theOtherLogo =
      el === 'updaterLogoHeader' ? 'partnerLogoHeader' : 'updaterLogoHeader'
    const bothLogosAreOff = !!(
      !this.state[el][0] === false && this.state[theOtherLogo][0] === false
    )
    return bothLogosAreOff
  }

  toggleElem = el => {
    const currentState = [this.state[el][0], this.state[el][1]]
    if (this.bothLogosOffInHeader(el)) {
      if (this.state.homePerksLogoHeader) {
        return
      }
      alert('At least one logo needs to be present')
      this.setState({
        updaterLogoHeader: [true],
        partnerLogoHeader: [false],
        poweredByUpdaterFooter: [false],
      })
    } else {
      this.setState({ [el]: [!currentState[0], currentState[1]] })
    }
  }

  showImgInfo = e => {
    e.target.style.transform = 'scale(2) rotate(5deg)'
    e.target.style.boxShadow = '0 1px 10px rgba(0,0,0,.3)'
    this.setState({
      imgInfoVisible: true,
      imgWidth: e.target.width,
      imgHeight: e.target.height,
    })
  }

  hideImgInfo = e => {
    e.target.style.transform = 'scale(1) rotate(0deg)'
    e.target.style.boxShadow = 'none'
    this.setState({ imgInfoVisible: false })
  }

  render() {
    const {
      mainHeadline,
      recipientName,
      updaterLogoHeader,
      partnerLogoHeader,
      homePerksLogoHeader,
      separateHomePerks,
      showLogin,
      partnerID,
      headerCTA,
      mainSubtitle,
      metricSubheading,
      metricDate,
      titleImage1,
      titleImage2,
      introTextTitle,
      introTextBody,
      introCTA,
      simpleTextBlock,
      metricContent,
      timelineBlock,
      timelineBlockProminentUpFirst,
      timeLineNextStepsHidden,
      callCTABlock,
      moverPackBlock,
      moverPackHighlight,
      homePerksBlock,
      twoUpImageAndText,
      twoUpImageAndTextContent1,
      twoUpImageAndTextContent2,
      oneUpImageAndText,
      oneUpImageAndTextContent1,
      oneUpImageAndTextContent2,
      oneUpImageAndTextContent3,
      oneUpImageAndTextSmall,
      oneUpImageAndTextSmallContent1,
      oneUpImageAndTextSmallContent2,
      oneUpImageAndTextSmallContent3,
      closingTextBlock,
      closingTextBlockWhiteBg,
      closingTextBlockCentered,
      closingTextBlockUmark,
      closingTextTitle,
      closingTextTitleSmall,
      closingTextBody,
      cobrandingPro,
      cobrandingSite,
      cobrandingContextual,
      cobrandingCobranding,
      cobrandingProfessional,
      cobrandingCoProfessional,
      poweredByUpdaterFooter,
      footerLegalBlock,
      homePerksFooter,
      otherOptions,
    } = this.state
    const { templateTitle, templateOptions, templateDescription } = this.props
    return (
      <React.Fragment>
        <Helmet>
          <title>{templateTitle}</title>
          <meta property="og:title" content={templateTitle} />
          <meta name="twitter:title" content={templateTitle} />
          <meta name="description" content={templateDescription} />
          <meta name="twitter:description" content={templateDescription} />
          <meta
            property="og:image"
            content="https://res.cloudinary.com/updater-marketing/image/upload/v1575902949/email_revamp/test-assets/ketchup-card.png"
          />
          <meta
            name="twitter:image"
            content="https://res.cloudinary.com/updater-marketing/image/upload/v1575902949/email_revamp/test-assets/ketchup-card.png"
          />
          <meta
            name="image"
            content="https://res.cloudinary.com/updater-marketing/image/upload/v1575902949/email_revamp/test-assets/ketchup-card.png"
          />
        </Helmet>
        <ImageInfoBlock
          width={this.state.imgWidth}
          height={this.state.imgHeight}
          visible={this.state.imgInfoVisible}
        />
        <EmailConfig
          toggleElem={this.toggleElem}
          mainHeadline={mainHeadline}
          recipientName={recipientName}
          updaterLogoHeader={updaterLogoHeader}
          partnerLogoHeader={partnerLogoHeader}
          showLogin={showLogin}
          homePerksLogoHeader={homePerksLogoHeader}
          separateHomePerks={separateHomePerks}
          mainSubtitle={mainSubtitle}
          metricSubheading={metricSubheading}
          metricDate={metricDate}
          titleImage1={titleImage1}
          titleImage2={titleImage2}
          introTextTitle={introTextTitle}
          introTextBody={introTextBody}
          introCTA={introCTA}
          simpleTextBlock={simpleTextBlock}
          metricContent={metricContent}
          timelineBlock={timelineBlock}
          timelineBlockProminentUpFirst={timelineBlockProminentUpFirst}
          timeLineNextStepsHidden={timeLineNextStepsHidden}
          callCTABlock={callCTABlock}
          moverPackBlock={moverPackBlock}
          moverPackHighlight={moverPackHighlight}
          homePerksBlock={homePerksBlock}
          twoUpImageAndText={twoUpImageAndText}
          twoUpImageAndTextContent1={twoUpImageAndTextContent1}
          twoUpImageAndTextContent2={twoUpImageAndTextContent2}
          oneUpImageAndText={oneUpImageAndText}
          oneUpImageAndTextContent1={oneUpImageAndTextContent1}
          oneUpImageAndTextContent2={oneUpImageAndTextContent2}
          oneUpImageAndTextContent3={oneUpImageAndTextContent3}
          oneUpImageAndTextSmall={oneUpImageAndTextSmall}
          oneUpImageAndTextSmallContent1={oneUpImageAndTextSmallContent1}
          oneUpImageAndTextSmallContent2={oneUpImageAndTextSmallContent2}
          oneUpImageAndTextSmallContent3={oneUpImageAndTextSmallContent3}
          closingTextBlock={closingTextBlock}
          closingTextBlockWhiteBg={closingTextBlockWhiteBg}
          closingTextBlockCentered={closingTextBlockCentered}
          closingTextBlockUmark={closingTextBlockUmark}
          closingTextTitle={closingTextTitle}
          closingTextTitleSmall={closingTextTitleSmall}
          closingTextBody={closingTextBody}
          cobrandingPro={cobrandingPro}
          cobrandingSite={cobrandingSite}
          cobrandingContextual={cobrandingContextual}
          cobrandingCobranding={cobrandingCobranding}
          cobrandingProfessional={cobrandingProfessional}
          cobrandingCoProfessional={cobrandingCoProfessional}
          poweredByUpdaterFooter={poweredByUpdaterFooter}
          footerLegalBlock={footerLegalBlock}
          homePerksFooter={homePerksFooter}
        />
        <OptionSelector
          templateTitle={templateTitle}
          templateOptions={templateOptions}
        />
        <EmailShell>
          <HeaderSection
            partnerLogoHeader={partnerLogoHeader}
            updaterLogoHeader={updaterLogoHeader}
            homePerksLogoHeader={homePerksLogoHeader}
            separateHomePerks={separateHomePerks}
            showLogin={showLogin}
            partnerID={partnerID}
            headerCTA={headerCTA}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <IntroSection
            mainHeadline={mainHeadline}
            recipientName={recipientName}
            mainSubtitle={mainSubtitle}
            metricSubheading={metricSubheading}
            metricDate={metricDate}
            titleImage1={titleImage1}
            titleImage2={titleImage2}
            introTextTitle={introTextTitle}
            introTextBody={introTextBody}
            introCTA={introCTA}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <SimpleTextBlock simpleTextBlock={simpleTextBlock} />
          <MonthlyMetricSection
            metricContent={metricContent}
            otherOptions={otherOptions}
          />
          <TimelineSection
            timelineBlock={timelineBlock}
            timelineBlockProminentUpFirst={timelineBlockProminentUpFirst}
            timeLineNextStepsHidden={timeLineNextStepsHidden}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
            otherOptions={otherOptions}
          />
          <CallCTASection
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
            callCTABlock={callCTABlock}
          />
          <MoverPackSection
            moverPackBlock={moverPackBlock}
            moverPackHighlight={moverPackHighlight}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <HomePerksSection homePerksBlock={homePerksBlock} />
          <TwoUpImageAndText
            twoUpImageAndText={twoUpImageAndText}
            twoUpImageAndTextContent1={twoUpImageAndTextContent1}
            twoUpImageAndTextContent2={twoUpImageAndTextContent2}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <OneUpImageAndText
            oneUpImageAndText={oneUpImageAndText}
            oneUpImageAndTextContent1={oneUpImageAndTextContent1}
            oneUpImageAndTextContent2={oneUpImageAndTextContent2}
            oneUpImageAndTextContent3={oneUpImageAndTextContent3}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <OneUpImageAndTextSmall
            oneUpImageAndTextSmall={oneUpImageAndTextSmall}
            oneUpImageAndTextSmallContent1={oneUpImageAndTextSmallContent1}
            oneUpImageAndTextSmallContent2={oneUpImageAndTextSmallContent2}
            oneUpImageAndTextSmallContent3={oneUpImageAndTextSmallContent3}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <CobrandingSection
            cobrandingPro={cobrandingPro}
            cobrandingSite={cobrandingSite}
            cobrandingContextual={cobrandingContextual}
            cobrandingCobranding={cobrandingCobranding}
            cobrandingProfessional={cobrandingProfessional}
            cobrandingCoProfessional={cobrandingCoProfessional}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
          <ClosingTextSection
            closingTextBlock={closingTextBlock}
            closingTextBlockWhiteBg={closingTextBlockWhiteBg}
            closingTextBlockCentered={closingTextBlockCentered}
            closingTextBlockUmark={closingTextBlockUmark}
            closingTextTitle={closingTextTitle}
            closingTextTitleSmall={closingTextTitleSmall}
            closingTextBody={closingTextBody}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />

          <FooterSection
            footerLegalBlock={footerLegalBlock}
            poweredByUpdaterFooter={poweredByUpdaterFooter}
            homePerksFooter={homePerksFooter}
            showInfo={this.showImgInfo}
            hideInfo={this.hideImgInfo}
          />
        </EmailShell>
      </React.Fragment>
    )
  }
}

export default Layout
